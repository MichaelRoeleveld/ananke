#define _GNU_SOURCE
#include <stdio.h> 
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>


typedef struct {
	char *head;
	char *file;
	int line;
} Lexer;


static inline void die(Lexer l, char *err, ...){
	printf("%s:%d ERROR ", l.file, l.line);
	va_list myargs;
	va_start(myargs,err);
	vprintf(err,myargs);
	putchar('\n');
	exit(1);
}

static inline void trim(Lexer *l){
	// line comment
	if(*l->head == ';'){
		l->head = strchrnul(l->head, '\n');
		trim(l);
	}
	// spaces
	if(isspace(*l->head)){
		if (*l->head == '\n'){
			l->line++;
		}
		l->head++;
		trim(l);
	}
	// multiline comment
	if(strncmp("/.", l->head, 2)==0){
		l->head = strstr(l->head+2, "./")+2;
		trim(l);
	}
}

static inline void expect(int x, Lexer *l, char *err, ...){
	if (x) return;
	printf("%s:%d ERROR ", l->file, l->line);
	va_list myargs;
	va_start(myargs,err);
	vprintf(err,myargs);
	printf(" (got '%.*s')\n", 10, l->head);
	exit(1);
}

static inline int take(Lexer *l, char *pat){
	int len = strlen(pat);
	if (strncmp(pat, l->head, len)){
		return 0;
	}
	l->head += len;
	trim(l);
	return 1;
}

static inline int take_name(Lexer *l, char **dest){
	char *start = l->head;
	if(!(*l->head=='_' || isalpha(*l->head))){
		return 0;
	}
	do{
		l->head++;
	}while(isalnum(*l->head) || *l->head=='_');
	trim(l);
	int len = l->head-start-1;
	char *str = malloc(len+1);
	str[len] = '\0';
	memcpy(str, start, len);
	*dest = str;
	return 1;
}

static inline int take_word(Lexer *l, char *word){
	int len = strlen(word);
	char *match = NULL;
	if (!take_name(l, &match)){return 0;};
	int ret = strncmp(match, word, len)==0;
	free(match);
	return ret;
}

static inline int take_number(Lexer *l, char **dest){
	char *start = l->head;
	while(isdigit(*l->head)){
		l->head++;
	}
	if(start == l->head){return 0;}
	if(*l->head == '.'){
		char *restore = l->head;
		l->head++;
		while(isdigit(*l->head)){
			l->head++;
		}
		if (l->head-1 == restore){
			l->head = restore;
		}
	}
	int len = l->head-start;
	*dest = malloc(len+1);
	(*dest)[len] = '\0';
	memcpy(*dest, start, len);
	trim(l);
	return 1;
}

static inline int take_term(Lexer *l){
	char *n;
	int r = take_name(l, &n) || take_number(l, &n);
	free(n);
	return r;
}

static inline int take_expr(Lexer *l){

}

static inline int parse_for(Lexer *l){
	if (!take(l, "for")){return 0;}
	char *name;
	expect(take_name(l, &name), l, "expected an identifier");
	expect(take_word(l, "in"), l, "expected `in`");
	take_expr(l);
	free(name);
	return 1;
}

int main(int argc, char const *argv[]){
	char *source = "ass 24.22310129sa for xd in 2..0 {asd}";
	Lexer lx = (Lexer){source, "inline", 0};
	Lexer *l = &lx;
	trim(l);
	//expect(take(&l, "for"), &l, "for");
	//parse_for(&l);
	/*
	eat(&l, "for");
	eat(&l, "x");
	eat(&l, "in");
	eat(&l, "2");
	eat(&l, "..");
	eat(&l, "0");*/
	return 0;
}