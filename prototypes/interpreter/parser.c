#include "tokenizer.c"

static const struct {
    //Token tok;
    signed char left_bp; // -1 if terminal
    signed char nud_bp; // -1 if no nud
    signed char led_bp; // -1 if no led
    signed char infix_led;
} bptab[NUM_TOKENS] = {
    [t_eof     ]={  0,  -1,  -1, 0},
    [t_string  ]={  0,  -1,  -1, 0}, // ""
    [t_ident   ]={  0,  -1,  -1, 0}, // blabla
    [t_float   ]={  0,  -1,  -1, 0}, // 1.1
    [t_int     ]={  0,  -1,  -1, 0}, // 1
    [t_assign  ]={ 10,  -1,  10, 1}, // =
    [t_and     ]={ 20,  -1,  20, 1}, // and
    [t_or      ]={ 20,  -1,  20, 1}, // or
    [t_xor     ]={ 20,  -1,  20, 1}, // xor
    [t_nor     ]={ 20,  -1,  20, 1}, // nor
    [t_not     ]={ 20,  20,  -1, 1}, // not
    [t_in      ]={ 30,  -1,  30, 1}, // in
    [t_comp    ]={ 30,  -1,  30, 1}, // <=>
    [t_lteq    ]={ 30,  -1,  30, 1}, // <=
    [t_gteq    ]={ 30,  -1,  30, 1}, // >=
    [t_neq     ]={ 30,  -1,  30, 1}, // !=
    [t_eq      ]={ 30,  -1,  30, 1}, // ==
    [t_lt      ]={ 30,  -1,  30, 1}, // <
    [t_gt      ]={ 30,  -1,  30, 1}, // >
    [t_plus    ]={ 40,  40,  40, 1}, // +
    [t_minus   ]={ 40,  40,  40, 1}, // -
    [t_concat  ]={ 40,  -1,  40, 1}, // ..
    [t_bor     ]={ 50,  -1,  50, 1}, // |
    [t_band    ]={ 60,  -1,  60, 1}, // &
    [t_tilde   ]={ 70,  -1,  70, 1}, // ~
    [t_lrot    ]={ 80,  -1,  80, 1}, // <<<
    [t_rrot    ]={ 80,  -1,  80, 1}, // >>>
    [t_lshift  ]={ 80,  -1,  80, 1}, // <<
    [t_rshift  ]={ 80,  -1,  80, 1}, // >>
    [t_mult    ]={ 80,  -1,  80, 1}, // *
    [t_mmult   ]={ 80,  -1,  80, 1}, // **
    [t_div     ]={ 80,  -1,  80, 1}, // /
    [t_div2    ]={ 80,  -1,  80, 1}, // /%
    [t_floordiv]={ 80,  -1,  80, 1}, // //
    [t_mod     ]={ 80,  -1,  80, 1}, // %
    [t_mod2    ]={ 80,  -1,  80, 1}, // %%
    [t_incr    ]={ 90,  -1,  90, 0}, // ++
    [t_decr    ]={ 90,  -1,  90, 0}, // --
    [t_pow     ]={ 90,  -1,  89, 1}, // ^
    [t_bnot    ]={ 90,  90,  -1, 0}, // !
    [t_as      ]={100,  -1, 100, 1}, // as
    [t_to      ]={100,  -1, 100, 1}, // to
    [t_coalesce]={110,  -1, 110, 1}, // ??
    [t_safe    ]={110,  -1, 110, 0}, // ?
    [t_addr    ]={110,  -1, 110, 0}, // $
    [t_deref   ]={110,  -1, 110, 0}, // @
    [t_dot     ]={110,  -1, 110, 1}, // .
    [t_lbracket]={110,   0,   0, 1}, // [
    [t_lparen  ]={110,   0,   0, 1}, // (
    [t_lbrace  ]={110,   0,  -1, 1}, // {
    [t_rbracket]={ -1,  -1,  -1, 0}, // ]
    [t_rparen  ]={ -1,  -1,  -1, 0}, // )
    [t_rbrace  ]={ -1,  -1,  -1, 0}, // }
    [t_comma   ]={ -1,  -1,  -1, 0}, // ,
    [t_for     ]={110,   0,  -1, 0}, // for
};

typedef struct __attribute__((packed)) AST {
    Lexeme lxm;
    enum {
        k_error,
        k_if,
        k_while,
        k_body,
        k_for,
        k_term,
        k_unop,
        k_binop,
        k_empty
    } kind;
    union {
        struct {
            int num_children;
            struct AST *children;
        } succ;
        const char *error;
    };
} AST;

// debug function for printing out the AST
void AST_dump(AST a, int depth){
    const char *indent = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; // scuffed
    const char *kinds[] = {"erronous", "if", "while", "body", "for", "term", "un op", "binop", "empty"};
    printf("%.*s%s: ",depth,indent,kinds[a.kind]);print_lexeme(a.lxm);
    if(a.succ.children != NULL){
        for(int i=0; i<a.succ.num_children; i++){
            AST_dump(a.succ.children[i], depth+1);
        }
    }
    if (a.kind == k_term){
        if (a.lxm.token == t_string)
            free(a.lxm.value.v_string);
        else
            if (a.lxm.token == t_ident)
                free(a.lxm.value.v_ident.name);
    }
}

typedef AST((*parser_list[])(Lexer*, Table *c));

static AST parse_if(Lexer *l, Table *c);
static AST parse_while(Lexer *l, Table *c);
static AST parse_any_of(Lexer *l, Table *c, int n, parser_list);
static AST parse_expr(Lexer *l, Table *c, int rbp);
static AST parse_body(Lexer *l, Table *c);
static AST parse_for(Lexer *l, Table *c);
static AST parse_fn(Lexer *l, Table *c);
static AST parse_nud(Lexer *l, Table *c);
static AST parse_led(Lexer *l, Table *c, AST left);

static AST AST_mem_region[512];
static AST *AST_mem_head = &AST_mem_region[0];
static inline AST *malloc_AST(int n){
    AST *tmp = AST_mem_head;
    AST_mem_head += n;
    return tmp;
}
static inline AST *realloc_AST(AST* old, int old_n, int new_n){
    if (AST_mem_head == old){
        AST_mem_head += new_n - old_n;
        return old;
    }
    AST *new = malloc_AST(new_n);
    for (int i=0; i<old_n; i++){
        new[i] = old[i];
    }
    return new;
}

static inline AST parse_expr(Lexer *l, Table *c, int rbp){
    Lexeme start = l->curr;
    AST left = parse_nud(l,c);
	// lbp < rbp means the parent caller will handle that operator
	// led_bp == -1 means not infix/postfix
	// an opening ([{ starting on a different line is never infix
    
    while (bptab[l->curr.token].left_bp > rbp
        && bptab[l->curr.token].led_bp != -1
		&&!(l->curr.pos.line != start.pos.line
			&&(l->curr.token == t_lbracket
			|| l->curr.token == t_lbrace
			|| l->curr.token == t_lparen
		))){
        left = parse_led(l, c, left);
    }
    return left;
}

static inline AST parse_nud(Lexer *l, Table *c){
    Lexeme lxm = l->curr;
    switch(l->curr.token){
        case t_if:
        return parse_if(l,c);
        case t_while:
        return parse_while(l,c);
        case t_for:
        return parse_for(l,c);
        case t_lbrace:
        return parse_body(l,c);
        case t_lparen:{
            lexer_consume(l,c);
            if (l->curr.token == t_rparen){
                lexer_consume(l,c);
                return (AST){l->curr, k_empty, .succ={0, NULL}};
            }
            AST x = parse_expr(l,c,0);
            lexer_consume(l,c);
            return x;
        }

        case t_ident:
        case t_int:
        case t_float:
        case t_string:
        lexer_consume(l,c);
        return (AST){lxm, k_term, .succ={0}};
        default:;
    }

    if (bptab[l->curr.token].nud_bp == -1){
        return (AST){l->curr, k_error, .error="token is not a prefix operator"};
    }
    lexer_consume(l,c); // eat the operator
    AST *child = malloc_AST(1);
    *child = parse_expr(l,c,0);
    return (AST){lxm, k_unop, .succ={1, child}};
}

static inline AST parse_led(Lexer *l, Table *c, AST left){
    if (bptab[l->curr.token].led_bp == -1){
        return (AST){l->curr, k_error, .error="token is not a postfix/infix operator"};
    }
    Lexeme lxm = l->curr;
    lexer_consume(l,c); // eat the operator
    AST *children = malloc_AST(2);
    children[0] = left;
    if (bptab[lxm.token].infix_led){
        children[1] = parse_expr(l,c,bptab[lxm.token].led_bp);
        if (lxm.token == t_lparen){
            if (l->curr.token == t_rparen){
                lexer_consume(l,c);
            } else {
                puts("expected ) but didn't find it here");
            }
        }
        return (AST){lxm, k_binop,.succ={2,children}};
    }
    return (AST){lxm, k_unop,.succ={1,children}};
}

static inline AST parse_any_of(Lexer *l, Table *c, int num_parsers, parser_list parsers) {
    for (int i = 0; i < num_parsers; ++i) {
        Lexer tmp = *l;
        AST result = parsers[i](&tmp, c);
        if (result.kind != k_error){
            *l = tmp;
            return result;
        }
    }
    puts("could not parse any of");
    return (AST){l->curr, k_error, .error="none of the parsers matched anything"};
}

static inline AST parse_body(Lexer *l, Table *c){
    Lexeme start = l->curr;
    if (l->curr.token != t_lbrace){
        return (AST){l->curr, k_error, .error="body needs to start with `{`"};
    }
    lexer_consume(l,c);
    AST *children = 0;
    int num_children = 0;
    while (l->curr.token != t_rbrace){
        if (l->curr.token == t_eof)
            return (AST){l->curr, k_error,.error="unexpected end of code"};
        children = realloc_AST(children, num_children, num_children+1);
        children[num_children++] = parse_expr(l,c,0);
    }
    lexer_consume(l,c);
    return (AST){start, k_body, .succ={num_children, children}};
}

static inline AST parse_for(Lexer *l, Table *c){
    Lexeme start = l->curr;
    if (l->curr.token != t_for){
        char *error = "the correct syntax is `for <ident> in <expr> <expr>`";
        return (AST){l->curr, k_error, .error=error};
    }
    lexer_consume(l,c);
    if (l->curr.token != t_ident){
        return (AST){l->curr, k_error, .error="expected an ident"};
    }
    int num_children = 3;
    AST *children = malloc_AST(3);
    children[0] = (AST){l->curr, k_term}; // x
    lexer_consume(l,c);
    if (l->curr.token != t_in){
        return (AST){l->curr, k_error, .error="expected `in`"};
    }
    lexer_consume(l,c);
    children[1] = parse_expr(l,c,0); // xs
    children[2] = parse_expr(l,c,0); // body
    return (AST){start, k_for, .succ={num_children, children}};
}

static inline AST parse_if(Lexer* l, Table *c) {
    if (l->curr.token != t_if){
        return (AST){l->curr, k_error, .error="the correct syntax is `if <cond> <expr>`"};
    }
    lexer_consume(l,c);
    AST *children = malloc_AST(2);
    children[0] = parse_expr(l,c,0); // cond
    children[1] = parse_expr(l,c,0); // body
    return (AST){l->curr, k_if, .succ={2, children}};
}

static inline AST parse_while(Lexer *l, Table *c) {
    Lexeme old = l->curr;
    if (l->curr.token != t_while){
        return (AST){l->curr, k_error, .error="the correct syntax is `while cond expr`"};
    }
    lexer_consume(l,c);
    AST *children = malloc_AST(2);
    children[0] = parse_expr(l,c,0); // cond
    children[1] = parse_expr(l,c,0); // body
    return (AST){old, k_while, .succ={2, children}};
}

AST parse_prog(const char *prog, const char *name){
    Table context;
    tab_init(&context, 64);

    Lexer l = lex(prog, name);
    lexer_consume(&l,&context); // prime lexer for pratt parsing
    while (l.curr.token != t_eof){
        AST x = parse_expr(&l,&context,0);
        AST_dump(x,0);
        if (x.kind == k_error){
            break;
        }
    }

    free(context.arr);
    return (AST){0};
}
