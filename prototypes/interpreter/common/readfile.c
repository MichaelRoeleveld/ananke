#include <stdlib.h>
#include <stdio.h>

int readfile(
	const char* restrict* src,
	size_t* sourceLen,
	const char* restrict filename
){
	FILE* sourceF = fopen(filename, "rb");
	if (sourceF == NULL){
		return 0;
	}
	fseek(sourceF, 0L, SEEK_END);
	*sourceLen = ftell(sourceF);
	*src = calloc(1, *sourceLen+1);
	rewind(sourceF);
	fread(*(void**)src, *sourceLen, 1UL, sourceF);
	fclose(sourceF);
	return 1;
}
