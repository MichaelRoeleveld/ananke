#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include "string.c"

typedef enum {
	t_none=0,
	/* length:count [keywords] */
	/* 2:9  */ t_as, t_co, t_do, t_fn, t_if, t_in, t_op, t_or, t_to,
	/* 3:8  */ t_and, t_for, t_let, t_nil, t_str, t_use, t_var, t_xor,
	/* 4:11 */ t_atom, t_bool, t_elif, t_else, t_goto, t_pure, t_true, t_type, t_unit, t_void, t_with,
	/* 5:9  */ t_async, t_await, t_break, t_exact, t_false, t_macro, t_match, t_where, t_while,
	/* 6:8  */ t_bigend, t_export, t_import, t_litend, t_module, t_return, t_static, t_thread,
	/* 7:1  */ t_garbage,
	/* 8:2  */ t_continue, t_volatile,
	t_comment,
	t_ident,t_decimal_lit,t_int_lit,t_string_lit,
	t_lparen, t_rparen, t_lbrace, t_rbrace, t_lbracket, t_rbracket, t_lt, t_gt, t_assign,
	t_dot, t_slash, t_comma, t_tilde, t_exclam, t_quest, t_monkeytail, t_hash, t_dollar, t_percent, t_caret, t_et, t_star, t_dash, t_plus, t_bar, t_colon,
	t_ddot,
	t_dpercent,
	t_dstar,
	t_dcolon,
	t_dquest,
	t_ddiv,
	t_equal,
	t_gteq,
	t_lteq,
	t_rshift,
	t_lshift,
	t_rrot,
	t_lrot,
	t_lteqgt
} Token;

typedef struct {
	size_t line, column;
	string filename;
} Location;

typedef struct {
	Token token;
	Location loc;
	string str;
} Lexeme;

Token whichKeyword(string ident){
	static const char *keys[10] = {
		"easter","egg",
		"ascodofnifinoporto",
		"andforletnilstrusevarxor",
		"atomboolelifelsegotopuretruetypeunitvoidwith",
		"asyncawaitbreakexactfalsemacromatchwherewhile",
		"bigendexportimportlitendmodulereturnstaticthread",
		"garbage",
		"continuevolatile"
	};

	// LUT for no. of keywords with length [idx]
	static int numkwds[9] = {0, 0, 9, 8, 11, 9, 8, 1, 2};

	// LUT for enum value offset of keyword in bucket [idx]
	static int offsets[9] = {0, 0, t_as, t_and, t_atom, t_async, t_bigend, t_garbage, t_continue};

	const size_t   len = ident.len;
	const char    *str = ident.str;
	const char *bucket = keys[len];

	size_t ch = 0, kwd = 0;
	while (ch < len && kwd < numkwds[len]) {
		const int diff = bucket[kwd*len+ch] - tolower(str[ch]);
		if (diff > 0){
			// letter_kwd > letter_str
			// sorted means we will never reach keyword now
			return t_ident;
		}
		else if (diff < 0){
			// letter_kwd < letter_str
			// keyword may be somewhere in front of us
			kwd++;
		}
		else {
			// letters match
			if (++ch == len){
				// last letter mean a complete keyword!
				return (Token)(offsets[len]+kwd);
			}
		}
	}

	return t_ident;
}

size_t scanNumber(string src, Token *out){
	const char *code = src.str;
	const size_t codeLen = src.len;

	int isFloat = 0;
	size_t numLen = 0;
	do {
		switch(code[numLen]){
			case ' ': goto end_number;
			case '_': if (!numLen) goto end_number; else break;
			case '.': if (isFloat) goto end_number; else {
				isFloat = 1;
				break;
			}

			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
			break;

			default: goto end_number;
		}
	} while (numLen++ < codeLen);

	end_number:
	if (numLen > 0 && code[numLen-1]=='.'){
		// for uniform call syntax, we must not count a trailing "."
		isFloat = 0;
		numLen--;
	}

	*out = isFloat?t_decimal_lit:t_int_lit;
	return numLen;
}

size_t scanIdentifierOrKeyword(string src, Token *out){
	const char *code = src.str;
	const size_t codeLen = src.len;

	size_t len = 0;
	while (len < codeLen){
		char c = code[len];
		if (isalpha(c) || (isdigit(c) && len>0) || c == '_'){
			len++;
		} else {
			break;
		}
	}

	Token keyw = whichKeyword( (string){.str = code, .len = len} );
	if (keyw){
		*out = keyw;
	} else {
		*out = t_ident;
	}

	return len;
}

size_t scanOperator(string src, Token *out){
	static struct{
		string s;
		Token t;
	} lut[] = {
		{{3,"<<<"},t_lrot},
		{{3,">>>"},t_rrot},
		{{3,"<=>"},t_lteqgt},
		{{2,".."},t_ddot},
		{{2,"%%"},t_dpercent},
		{{2,"**"},t_dstar},
		{{2,"::"},t_dcolon},
		{{2,"??"},t_dquest},
		{{2,"//"},t_ddiv},
		{{2,"=="},t_equal},
		{{2,">="},t_gteq},
		{{2,"<="},t_lteq},
		{{2,">>"},t_rshift},
		{{2,"<<"},t_lshift},
	};
	size_t l = sizeof(lut)/sizeof(lut[0]);
	for (size_t i=0; i<l; i++){
		if (str_startswith(src, lut[i].s)){
			*out = lut[i].t;
			return lut[i].s.len;
		}
	}
	return 0;
}

size_t scanString(string src, Token *out){
	if (src.str[0] != '"') {
		return 0;
	}

	for (size_t i=1; i<src.len; i++){
		if (src.str[i] == '"'){
			*out = t_string_lit;
			return i+1;
		}
	}

	return 0;
}

size_t scanComment(string src, Token *out){
	size_t len = 0;
	if (src.len >= 4 && src.str[0]=='/' && src.str[1]=='.'){
		int nest = 0;
		for (size_t i=3; i<src.len; ++i){
			if (src.str[i-1]=='.' && src.str[i]=='/' && nest--==0){
				len = i+1;
				*out = t_comment;
				return len;
			} else
			if (src.str[i-1]=='/' && src.str[i]=='.'){
				nest++;
			}
		}
	}
	return len;
}

typedef struct {
	Lexeme *arr;
	size_t len,cap;
} LexemeArr;

LexemeArr lex (string src){
	
	size_t line = 1;
	size_t col = 1;

	LexemeArr lexemes = {calloc(64,sizeof(Lexeme)), 0, 64};

	for (size_t i=0; i<src.len;){
		Token tok = t_none;
		string sub = substr(src, i, 0);
		size_t len = 1;
		
		// scan* does not mutate sub, hence no body after the ifs
		if (len = scanIdentifierOrKeyword(sub, &tok)){}
		else if (len = scanNumber(sub, &tok)){}
		else if (len = scanString(sub, &tok)){}
		else if (len = scanOperator(sub, &tok)){}
		else if (len = scanComment(sub, &tok)){}
		else {
			static Token lut[UCHAR_MAX] = {
				t_none,
				['('] = t_lparen,   [')'] = t_rparen,
				['{'] = t_lbrace,   ['}'] = t_rbrace,
				['['] = t_lbracket, [']'] = t_rbracket,
				['<'] = t_lt,       ['>'] = t_gt,
				['.'] = t_dot,      [','] = t_comma,
				['*'] = t_star,     ['/'] = t_slash,
				['~'] = t_tilde,    ['^'] = t_caret,
				['!'] = t_exclam,   ['?'] = t_quest,
				['#'] = t_hash,     ['@'] = t_monkeytail,
				['$'] = t_dollar,   ['%'] = t_percent,
				['&'] = t_et,       ['|'] = t_bar,
				['+'] = t_plus,     ['-'] = t_dash,
				['='] = t_assign,   [':'] = t_colon,
			};

			if ((tok = lut[src.str[i]]) == t_none){
				switch(src.str[i]){
					case '\n': col=0; line++;
					case '\t':
					case ';':
					case ' ':
					break;

					default:
					printf("<! illegal character '\\x%02x' ignored !>",(unsigned char)src.str[i]);
					break;
				}
			}

			len = 1;
		}
		if (tok != t_none && tok != t_comment){
			//printf("main.ank:%02llu:%02llu: %02d(% 2llu): %.*s\n", line, col, tok, len, (int)len, sub.str);
			lexemes.arr[lexemes.len++] = (Lexeme){
				.token = tok,
				.loc={.line=line, .column=col, (string){
					.str="main.ank",
					.len=8}},
				.str = substr(sub, 0, len)
			};
			if (lexemes.len == lexemes.cap){
				lexemes.cap *= 2;
				lexemes.arr = realloc(lexemes.arr, sizeof(Lexeme)*lexemes.cap);
			}
		}
		i += len;
		col += len;
	}
	return lexemes;
}

void test(){
	struct {
		string input;
		Token expected;
	} tests[] = {
		{{0, ""}, t_ident},
		{{1, "x"}, t_ident},
		{{2, "as"}, t_as},
		{{2, "to"}, t_to},
		{{3, "len"}, t_ident},
		{{3, "let"}, t_let},
		{{3, "lez"}, t_ident},
		{{3, "xor"}, t_xor},
		{{3, "XOR"}, t_xor},
		{{4, "void"}, t_void},
		{{4, "with"}, t_with},
		{{5, "doggy"}, t_ident},
		{{5, "false"}, t_false},
		{{5, "while"}, t_while},
		{{6, "awhile"}, t_ident},
		{{6, "return"}, t_return},
		{{6, "letlet"}, t_ident},
		{{7, "garbage"}, t_garbage},
		{{8, "volatile"}, t_volatile},
		{{9, "shitpants"}, t_ident},
		{{9, "returnnil"}, t_ident}
	};

	size_t numtests = sizeof(tests)/sizeof(tests[0]);
	for (size_t i=0; i<numtests; i++){
		Token t = whichKeyword(tests[i].input);
		fprintf(stderr, "\"%.*s\": %s\n", (int)tests[i].input.len, tests[i].input.str, t!=t_ident?"keyword":"symbol");
		assert(t == tests[i].expected);
	}
	puts("====[ Passed all tests ]====");
}

int main(){
	test();
}