#include <string.h>
#include <stdlib.h>

typedef struct{
	size_t len;
	const char* str;
} string;

string substr(string super, size_t start, size_t end){
	if (end == 0){
		end = super.len;
	}
	return (string){.len = end-start, .str=&super.str[start]};
}

int str_startswith(string haystack, string needle){
	if (haystack.len < needle.len){
		return 0;
	}
	return strncmp(haystack.str, needle.str, needle.len)==0;
}

string newstr(const char *s){
	return (string){.len=strlen(s), .str=s};
}

// string str_copy(string x){
// 	char *tmp = calloc(x.len, sizeof(char));
// 	memcpy(tmp, x.str, x.len);
// 	return (string){x.len, tmp};
// }
