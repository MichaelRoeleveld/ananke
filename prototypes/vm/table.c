#ifndef TABLE
#define TABLE

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>

// This table uses a bounded linear probing scheme.
// Every lookup takes is O(PROBE_LEN) which is a constant, so O(1)
// When inserting, if it can not find a suitable spot within PROBE_LEN,
// The table is doubled in size and the elements are re-emplaced.
// Deletion uses a tombstone like mechanism. A hash value of 0 signals "empty".
// The design is such that the table does not own the data.
// When the table is freed, the keys and values are not freed, just the array of pairs is.
// The address of a Pair is not stable, and its nor its members' addresses should not be relied on.

//////////////////
// PUBLIC SYMBOLS

typedef uint64_t Hash;
// compare 2 keys for equality ((key, key) -> bool)
typedef bool (*const KeyEqFn )(const void*, const void*);
// hash a key (key -> hash)
typedef Hash (*const HashFn  )(const void*);
// when clearing a pair, perhaps also free the key and/or value. These can be NULL if no action is needed.
typedef void (*const KeyDelFn)(void*);
typedef void (*const ValDelFn)(void*);

typedef struct table Table;

// Make a new table
Table tableNew(KeyEqFn, HashFn, KeyDelFn, ValDelFn);

// Delete the table
void  tableDel(Table *table);

// Insert or update the value v associated with key k in table t
void  tableSet(Table *table, void *key, void *value);

// Retrieve the value associated with key k (output parameter r)
// Returns NULL if not found
void *tableGet(Table *table, void *key);

// Remove a key from the table
void  tableRem(Table *table, void *key);

/////////////////
// USAGE EXAMPLE

/*
#include <stdio.h>
#include <string.h>
#include "table.c"

bool strEq(const void *k1, const void *k2){
	return 0==strcmp((const char*)k1, (const char*)k2);
}

Hash hash(const void *k) {
	// Algorithm: FNV-1a 64 bit
	const uint64_t FNV_OFFSET_BASIS = 14695981039346656037ull;
	const uint64_t FNV_PRIME = 1099511628211ull;
	Hash h = FNV_OFFSET_BASIS;
	iter (i, 0, strlen((const char*)k)){
		h ^= ((unsigned char*)k)[i];
		h *= FNV_PRIME;
	}
	return h+1; // +1 to prevent accidentally outputting a tombstone
}

int main(void){
	Table t = tableNew(strEq, hash, NULL, NULL);
	tableSet(&t, "hello", &(int){4});
	tableSet(&t, "world", &(int){7});
	tableRem(&t, "hello");
	int *v;
	if ((v=tableGet(&t, "world")) != NULL){
		printf("w = %d\n", *v);
		// the value for "world" is *v
	} else {
		// "world" is not in the table
	}
	tableDel(&t);
}
*/

//////////////////////////
// IMPLEMENTATION DETAILS

// maximum probe length
#define PROBE_LEN 8
// iterate i from s to s+n
#define iter(i,s,n)  for(size_t _s=(s),_e=_s+(n),i=_s; i<_e; i++)
// modulo a hash with the table size
#define tabmod(h,t) ((h)&((t)->size-1))
// probe a table by a hash
#define probe(i,t,h) iter(i, tabmod(h,t), PROBE_LEN)

typedef struct {
	void *key, *val;
	Hash hash; // hash of key, 0 means cell uninhabited
} Pair;

struct table {
	Pair    *pairs;
	size_t   size;
	KeyEqFn  keyEq;
	HashFn   keyHash;
	KeyDelFn keyDel;
	ValDelFn valDel;
};

// is slot i of table t empty
static inline bool isEmpty(Table *t, size_t i){ return t->pairs[i].hash == 0; }

// is hash of slot i of table t equal to h
static inline bool hashEql(Table *t, size_t i, Hash h){ return t->pairs[i].hash == h; }

// is key of slot i of table t equal to k
static inline bool keysEql(Table *t, size_t i, void *k){ return t->keyEq(t->pairs[i].key, k); }

// no-op for key and value deletion
static inline void noopDel(void *_){}

Table tableNew(KeyEqFn ke, HashFn kh, KeyDelFn kd, ValDelFn vd){
	size_t size  = 32;
	return (Table){
		.size    = size,
		.pairs   = calloc(size+PROBE_LEN, sizeof *(Table){0}.pairs),
		.keyEq   = ke,
		.keyHash = kh,
		.keyDel  = kd,
		.valDel  = vd
	};
}

void tableDel(Table *t){
	KeyDelFn kd = t->keyDel ? t->keyDel : noopDel;
	ValDelFn vd = t->valDel ? t->valDel : noopDel;
	if (kd != noopDel && vd != noopDel){
		iter(i,0,t->size){
			if (!isEmpty(t,i)){
				kd(t->pairs[i].key);
				vd(t->pairs[i].val);
			}
		}
	}
	free(t->pairs);
	t->size  = 0;
	t->pairs = NULL;
}

void tableSet(Table *t, void *k, void *v){
	size_t h = t->keyHash(k);
	while(1){
		size_t firstEmpty = SIZE_MAX;
		probe(i,t,h){
			// Key already exists, update the value
			if (hashEql(t,i,h) && keysEql(t,i,k)){
				t->pairs[i] = (Pair){k,v,h};
				return;
			}
			// If we find an empty spot, the sought key might still be in a following spot because the table could be fragmented after deletion.
			if (isEmpty(t,i) && i < firstEmpty){
				firstEmpty = i;
			}
		}
		if (firstEmpty != SIZE_MAX){
			t->pairs[firstEmpty] = (Pair){k,v,h};
			return;
		}
		// couldn't find the key nor an empty spot in PROBE_LEN
		// double Table size and try again
		// recursive
		size_t oldSize  = t->size;
		Pair  *oldPairs = t->pairs;
		t->pairs = calloc((t->size*=2)+PROBE_LEN, sizeof *t->pairs);
		iter(i,0,oldSize+PROBE_LEN){
			if (!isEmpty(t,i)){
				tableSet(t, oldPairs[i].key, oldPairs[i].val);
			}
		}
		free(oldPairs);
		// try again to insert the new element in the next iteration
	}
}

void *tableGet(Table *t, void *k){
	size_t h = t->keyHash(k);
	probe(i,t,h){
		if (hashEql(t,i,h) && keysEql(t,i,k)){
			return t->pairs[i].val;
		}
	}
	return NULL;
}

void tableRem(Table *t, void *k){
	size_t h = t->keyHash(k);
	// todo: defragment table after removal so that the insertion routine can be simplified with the assumption that encountering an empty slot means the key doesn't exist
	probe(i,t,h){
		if (hashEql(t,i,h) && keysEql(t,i,k)){
			t->pairs[i].hash = 0; // mark uninhabited
			return;
		}
	}
}

#undef iter
#undef probe
#undef PROBE_LEN

#endif
