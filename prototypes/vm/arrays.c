// holds C code for the VM implementation of arrays, matrices and hashtables

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	size_t cap;
	size_t len;
	size_t elemSize;
	uint8_t *data;
} Array;

void arrInsert(Array *a, uint8_t *elem){
	if (a->len == a->cap){
		a->data = realloc(a->data, (a->cap*=2)*a->elemSize);
	}
	memcpy(&a->data[a->len++], elem, a->elemSize);
}

void arrInit(Array *a, size_t elemSize){
	a->data = calloc((a->cap=32), (a->elemSize=elemSize));
	a->len = 0;
}

void arrDelete(Array *a){
	free(a->data);
}