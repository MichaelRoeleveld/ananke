// asprintf
#define _GNU_SOURCE

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// #define DEBUG

#ifdef DEBUG
	#include <assert.h>
	#define debugprintf printf
#else
	#define assert(x)
	#define debugprintf(...)
#endif

// Bookkeeping a dynamic array after insertion.
#define PUSHED(s) do {\
		if (++s.top == s.cap)\
			s.arr = realloc(s.arr, (s.cap*=2)*sizeof *s.arr);\
	} while (0)

// Simplified example of augmenteed reference counting which deals with cyclical references.

// A Vertex would be a dynamically allocated object, which may contain pointers to other objects (outgoing references).
typedef struct vertex Vertex;
struct vertex {
	const char *name; // Name for pretty printing
	int rc;           // Incoming reference count
	int edges;        // Outgoing reference count
	Vertex **cons;    // Array of outgoing references
};

// Transient metadata bundle used during reachability analysis. Data 1:1 Vertex.
typedef struct {
	// The SCC to which the corresponding vertex belongs.
	unsigned scc;
	// Data used in Tarjan's Algorithm for finding strongly connected components.
	unsigned preorder, lowlink, onstack:1;
	// Data used in the final freeing depth first search, acts as "visited" marker.
	unsigned freed:1;
} Data;

// Table that pairs vertices to their respective metadata bundles, simple array of pairs, no hashing.
typedef struct {
	struct {
		const Vertex *v; // not owned
		Data *d; // owned by table
		// reason for pointer is smaller pairs[] size as well as allowing caching of data pointers in various points in the code, in a way that the pointers don't end up dangling after the table is grown.
	} *pairs;
	size_t cap,len;
} Table;

Table tableNew(void){ return (Table){.len=0, .cap=32, .pairs=calloc(32, sizeof (Table){0}.pairs[0])}; }

void tableDel(Table *t){
	for(size_t i=0; i<t->len; i++){
		free(t->pairs[i].d);
	}
	free(t->pairs);
}

// Retrieve the data bundle associated with a vertex.
// If the vertex is not yet in the table, init the bundle to 0 and return a pointer to that.
Data *tableGet(Table *t, Vertex *v){
	for(size_t i=0; i<t->len; i++){
		if (t->pairs[i].v == v){
			return t->pairs[i].d;
		}
	}
	// No entry yet, append it.
	if (t->len == t->cap){
		// Grow array.
		t->cap  *= 2;
		t->pairs = realloc(t->pairs, t->cap * sizeof t->pairs[0]);
	}
	t->pairs[t->len].v = v;
	t->pairs[t->len].d = calloc(1, sizeof *t->pairs[0].d);
	return t->pairs[t->len++].d;
}

// For unit testing purposes, allows using direct char* comparison instead of strcmp.
const char *const FREED = "FREED";

// Free an object.
void freeVertex(Vertex *v){
	debugprintf("freed %s\n", v->name);
	v->rc = 0;
	v->name = FREED;
}

int min(int a, int b){
	return a<b?a:b;
}

// In normal reference counting, once an object is freed, any referenced objects' reference counts are decremented.
// Here I have overloaded that function to do reachability analysis.
void decrement(Vertex *v){
	Table table = tableNew();
	debugprintf("doing reachability analysis from %s\n", v->name);
	
	// Needed by Tarjan's algorithm.
	struct {
		struct {
			Vertex *v;
			Data *d;
		}* arr;
		int cap, top;
	} stack = {.cap=32, .top=0, .arr=calloc(32, sizeof *stack.arr)};
	
	// Dynamic array of reference counts of strongly connected components.
	// sccRcs.top-1 is the amount of SCCs.
	// An SCC value of 0 is reserved as "no scc", hence top starts at 1.
	struct {
		int *arr;
		int cap, top;
	} sccRcs = {.cap=32, .top=1, .arr=calloc(32, sizeof *sccRcs.arr)};

	// Tarjan's algorithm for finding strongly connected components
	void tarjan(Vertex *v, Data *vd, int depth){
		assert(depth >= 1); // else a unitialized preorder and lowlink is indistinguishable from one filled in at the first generation, this leads to a bug where the final node is counted an extra time as its own SCC as well as being part of another SCC if that applies.
		vd->preorder = depth;
		vd->lowlink  = depth;
		vd->onstack  = 1;
		stack.arr[stack.top].v = v;
		stack.arr[stack.top].d = vd;
		PUSHED(stack);

		// Traverse neighbouring edges
		int e = v->edges;
		for (int i=0; i<e; i++){
			Vertex *w = v->cons[i];
			// Register in the table if not yet in it.
			Data *wd = tableGet(&table, w);

			// Black compsci magic
			if (wd->preorder == 0){
				tarjan(w, wd, depth+1);
				vd->lowlink = min(vd->lowlink, wd->lowlink);
			} else if (wd->onstack){
				vd->lowlink = min(vd->lowlink, wd->preorder);
			}
		}

		// Check if visiting this vertex wraps up an SCC
		if (vd->preorder == vd->lowlink){
			Vertex *w;
			Data   *wd;
			int     rc = 0;
			int oldtop = stack.top;
			debugprintf("SCC %d:", stack.top);
			do {
				// pop
				stack.top--;
				wd = stack.arr[stack.top].d;
				w  = stack.arr[stack.top].v;
				wd->onstack = 0;
				wd->scc = sccRcs.top;

				// Now for each edge out of this w, if the vertex it leads to is in the same scc, discount the ref.
				rc += w->rc;
				int e = w->edges;
				debugprintf(" %s", w->name);
				for(int i=0; i<e; i++){
					Vertex *neigh = w->cons[i];
					debugprintf("%s%s%c", i?"":"(->", neigh->name, i+1==e?')':',');
					// if the neighbour exists in the upper portion of the stack, it's in the same SCC
					int search = oldtop;
					do {
						search--;
						if (stack.arr[search].v == neigh){
							rc--;
							break;
						}
					} while (stack.arr[search].v != v);
				}
			} while (w != v);
			debugprintf("   - rc:%d\n",rc);
			sccRcs.arr[sccRcs.top] = rc;
			PUSHED(sccRcs);
		}
	}

	void decRecurse(Vertex *v, Data *vData, int theirSCC){
		// A preceding vertex from was just freed. Decrement the rc of the current vertex.
		v->rc--;
		debugprintf("decremented %s (rc:%d)\n", v->name, v->rc);
		assert(v->rc >= 0);
		// If the preceding vertex was from another SCC, decrement the SCC of the current vertex.
		int mySCC = vData->scc;
		if (theirSCC != mySCC){
			sccRcs.arr[mySCC]--;
			debugprintf("decremented SCC %d (rc:%d)\n", mySCC, sccRcs.arr[mySCC]);
		}
		assert(sccRcs.arr[mySCC] >= 0);
		if (sccRcs.arr[mySCC] == 0){
			// Don't end up decrementing a freed node.
			vData->freed = 1;
			// The SCC can be freed.
			debugprintf("SCC %d can be freed\n", mySCC);
			for (int i = 0; i < v->edges; i++){
				Vertex *neigh     = v->cons[i];
				Data   *neighData = tableGet(&table, neigh);
				if (!neighData->freed){
					decRecurse(neigh, neighData, mySCC);
				}
			}

			freeVertex(v);
		}
	}

	// Init the first table entry.
	Data *vData = tableGet(&table, v);

	// In the reference graph starting at v, find all strongly connected components.
	tarjan(v, vData, 1);

	// Decrement and free recursively (DFS)
	decRecurse(v, vData, 0);
	
	free(sccRcs.arr);
	free(stack.arr);
	tableDel(&table);
}

void test0(void){
	Vertex V = (Vertex){.name="V", .rc=2, .edges=1, .cons=(Vertex*[]){&V}};
	decrement(&V);
	assert(V.rc == 0 && V.name == FREED);
}

void test1(void){
	Vertex A,B,C;
	A = (Vertex){.name="START", .rc=1, .edges=1, .cons=(Vertex*[]){&B}};
	B = (Vertex){.name="CONNECT", .rc=1, .edges=1, .cons=(Vertex*[]){&C}};
	C = (Vertex){.name="LOOP", .rc=2, .edges=1, .cons=(Vertex*[]){&C}};
	decrement(&A);
	assert(A.rc == 0 && A.name == FREED);
	assert(B.rc == 0 && B.name == FREED);
	assert(C.rc == 0 && C.name == FREED);
}

void test2(void){
	Vertex S,A,B,X;
	// external -> X -> A -> B
	//               S,^
	S = (Vertex){.name="S", .rc=1, .edges=1, .cons=(Vertex*[]){&A}};
	X = (Vertex){.name="X", .rc=1, .edges=1, .cons=(Vertex*[]){&A}};
	A = (Vertex){.name="A", .rc=2, .edges=1, .cons=(Vertex*[]){&B}};
	B = (Vertex){.name="B", .rc=1, .edges=0, .cons=NULL};
	decrement(&S);
	assert(S.rc == 0 && S.name == FREED);
	assert(A.rc == 1 && A.name != FREED);
	assert(B.rc == 1 && B.name != FREED);
	assert(X.rc == 1 && X.name != FREED);
	decrement(&X);
	assert(S.rc == 0 && S.name == FREED);
	assert(A.rc == 0 && A.name == FREED);
	assert(B.rc == 0 && B.name == FREED);
	assert(X.rc == 0 && X.name == FREED);
}

void test3(void){
	Vertex I,J,K;
	// Fully conencted graph order 3
	I = (Vertex){.name="I", .rc=3, .edges=2, .cons=(Vertex*[]){&J, &K}};
	J = (Vertex){.name="J", .rc=2, .edges=2, .cons=(Vertex*[]){&I, &K}};
	K = (Vertex){.name="K", .rc=2, .edges=2, .cons=(Vertex*[]){&I, &J}};
	decrement(&I);
	assert(I.rc == 0 && I.name == FREED);
	assert(J.rc == 0 && J.name == FREED);
	assert(K.rc == 0 && K.name == FREED);
}

void test4(void){
	Vertex A,B,X,Y;
	// x -> a <-> b <- y
	A = (Vertex){.name="A", .rc=2, .edges=1, .cons=(Vertex*[]){&B}};
	B = (Vertex){.name="B", .rc=2, .edges=1, .cons=(Vertex*[]){&A}};
	X = (Vertex){.name="X", .rc=1, .edges=1, .cons=(Vertex*[]){&A}};
	Y = (Vertex){.name="Y", .rc=1, .edges=1, .cons=(Vertex*[]){&B}};
	decrement(&X);
	assert(A.rc == 1 && A.name != FREED);
	assert(B.rc == 2 && B.name != FREED);
	assert(X.rc == 0 && X.name == FREED);
	assert(Y.rc == 1 && Y.name != FREED);
	decrement(&Y);
	assert(A.rc == 0 && A.name == FREED);
	assert(B.rc == 0 && B.name == FREED);
	assert(X.rc == 0 && X.name == FREED);
	assert(Y.rc == 0 && Y.name == FREED);
}

void test5(void){
	Vertex L,M,N,O,P;
	// 2 fully connected components
	L = (Vertex){.name="L", .rc=4, .edges=4, .cons=(Vertex*[]){&L, &M, &N, &O}};
	M = (Vertex){.name="M", .rc=3, .edges=3, .cons=(Vertex*[]){&L, &M, &N}};
	N = (Vertex){.name="N", .rc=3, .edges=3, .cons=(Vertex*[]){&L, &M, &N}};
	O = (Vertex){.name="O", .rc=3, .edges=2, .cons=(Vertex*[]){&O, &P}};
	P = (Vertex){.name="P", .rc=2, .edges=2, .cons=(Vertex*[]){&O, &P}};
	decrement(&L);
	assert(L.rc == 0 && L.name == FREED);
	assert(M.rc == 0 && M.name == FREED);
	assert(N.rc == 0 && N.name == FREED);
	assert(O.rc == 0 && O.name == FREED);
	assert(P.rc == 0 && P.name == FREED);
}

void test6(void){
	// Bunch of random crap
	Vertex A,B,C,D,E,F,G,H,I,J,Q,R,S,T,U,V,W,X,Y,Z;
	R = (Vertex){.name="R", .rc=1, .edges=1, .cons=(Vertex*[]){&Q}};
	Q = (Vertex){.name="Q", .rc=2, .edges=1, .cons=(Vertex*[]){&R}};

	W = (Vertex){.name="W", .rc=4, .edges=4, .cons=(Vertex*[]){&T,&U,&V,&W}};
	V = (Vertex){.name="V", .rc=3, .edges=4, .cons=(Vertex*[]){&T,&U,&W,&Q}};
	U = (Vertex){.name="U", .rc=4, .edges=4, .cons=(Vertex*[]){&T,&U,&V,&W}};
	T = (Vertex){.name="T", .rc=4, .edges=3, .cons=(Vertex*[]){&U,&V,&W}};

	Z = (Vertex){.name="Z", .rc=1, .edges=1, .cons=(Vertex*[]){&X}};
	Y = (Vertex){.name="Y", .rc=1, .edges=1, .cons=(Vertex*[]){&Z}};
	X = (Vertex){.name="X", .rc=1, .edges=2, .cons=(Vertex*[]){&E,&H}};

	J = (Vertex){.name="J", .rc=1, .edges=0, .cons=NULL};
	I = (Vertex){.name="I", .rc=1, .edges=1, .cons=(Vertex*[]){&J}};
	H = (Vertex){.name="H", .rc=2, .edges=2, .cons=(Vertex*[]){&I,&G}};
	G = (Vertex){.name="G", .rc=2, .edges=1, .cons=(Vertex*[]){&H}};
	F = (Vertex){.name="F", .rc=1, .edges=1, .cons=(Vertex*[]){&G}};
	E = (Vertex){.name="E", .rc=3, .edges=1, .cons=(Vertex*[]){&Y}};
	D = (Vertex){.name="D", .rc=2, .edges=1, .cons=(Vertex*[]){&E}};
	C = (Vertex){.name="C", .rc=1, .edges=2, .cons=(Vertex*[]){&D,&E}};
	B = (Vertex){.name="B", .rc=1, .edges=1, .cons=(Vertex*[]){&C}};
	A = (Vertex){.name="A", .rc=1, .edges=2, .cons=(Vertex*[]){&B,&D}};

	S = (Vertex){.name="S", .rc=1, .edges=3, .cons=(Vertex*[]){&A,&F,&T}};
	decrement(&S);
	assert(S.rc == 0 && S.name == FREED);
	assert(A.rc == 0 && A.name == FREED);
	assert(B.rc == 0 && B.name == FREED);
	assert(C.rc == 0 && C.name == FREED);
	assert(D.rc == 0 && D.name == FREED);
	assert(E.rc == 0 && E.name == FREED);
	assert(F.rc == 0 && F.name == FREED);
	assert(G.rc == 0 && G.name == FREED);
	assert(H.rc == 0 && H.name == FREED);
	assert(I.rc == 0 && I.name == FREED);
	assert(J.rc == 0 && J.name == FREED);
	assert(Q.rc == 0 && Q.name == FREED);
	assert(R.rc == 0 && R.name == FREED);
	assert(T.rc == 0 && T.name == FREED);
	assert(U.rc == 0 && U.name == FREED);
	assert(V.rc == 0 && V.name == FREED);
	assert(W.rc == 0 && W.name == FREED);
	assert(X.rc == 0 && X.name == FREED);
	assert(Y.rc == 0 && Y.name == FREED);
	assert(Z.rc == 0 && Z.name == FREED);
}

void test7(void){
	// fully connected graph, order 6
	Vertex A,B,C,D,E,F,S;
	#define VERT(n, r, e, ...) n=(Vertex){.name=#n, .rc=r, .edges=e, .cons=((Vertex*[]){__VA_ARGS__})}
	#define ASSERTFREE(v) assert(v.rc==0 && v.name == FREED)
	VERT(S, 1, 1, &A);
	VERT(A, 6, 5, &B,&C,&D,&E,&F);
	VERT(B, 5, 5, &A,&C,&D,&E,&F);
	VERT(C, 5, 5, &A,&B,&D,&E,&F);
	VERT(D, 5, 5, &A,&B,&C,&E,&F);
	VERT(E, 5, 5, &A,&B,&C,&D,&F);
	VERT(F, 5, 5, &A,&B,&C,&D,&E);
	decrement(&S);
	ASSERTFREE(A);
	ASSERTFREE(B);
	ASSERTFREE(C);
	ASSERTFREE(D);
	ASSERTFREE(E);
	ASSERTFREE(F);
	ASSERTFREE(S);
}

void test8(void){
	// double references
	Vertex A,B,C,D,S;
	VERT(S, 1, 1, &A);
	VERT(A, 1, 2, &B,&B);
	VERT(B, 2, 2, &C,&C);
	VERT(C, 4, 2, &D,&D);
	VERT(D, 2, 2, &C,&C);
	decrement(&S);
	ASSERTFREE(S);
	ASSERTFREE(A);
	ASSERTFREE(B);
	ASSERTFREE(C);
	ASSERTFREE(D);
}

void test9(void){
	// n-degree fully connected and looping
	int n = 4096;
	Vertex **arr = calloc(n, sizeof *arr);
	char **names = calloc(n, sizeof *names);
	for(int i=0; i<n; i++){
		asprintf(&names[i], "v%d", i);
		arr[i] = malloc(sizeof *arr[i]);
		*arr[i] = (Vertex){.name=names[i], .edges=n, .rc=n, .cons=arr};
	}
	arr[0]->rc++;
	decrement(arr[0]);
	for(int i=0; i<n; i++){
		free(names[i]);
		assert(arr[i]->rc==0 && arr[i]->name==FREED);
		free(arr[i]);
	}
	free(names);
	free(arr);
}

int main() {
	void (*tests[])(void) = {
		test0, test1, test2, test3, test4, test5, test6, test7, test8, test9
	};
	for(size_t i=0; i<sizeof tests / sizeof *tests; i++){
		printf("\n! Running test %lu !\n", i);
		tests[i]();
	}

	return 0;
}
