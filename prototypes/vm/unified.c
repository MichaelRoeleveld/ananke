#include <stdlib.h>
#include <stdint.h>

typedef enum {
	NOP,
	CALL,
	RET,
	DEREF,
	ADDR,
	ADD,
	SUB,
	PUTS,
	JMP,
	BR
} Op;

/*
; addressing is current-frametop relative. The only other way to address is to build a pointer and use that.
; exception: jumps like BR, CALL, JMP are addressed absolutely in the code array.

; operand size is 64 bit.
; all operands are offsets into the current frame memory.

; function calling
; a frame is created
; arguments are copied over into the frame
; as well as a PTR to the return value(s) destination
; as well as a PTR to the captures in case of a closure
; context is set to the new frame
; control flow is set to the function
; the function reads from its local copy of arguments
; the function reads/writes from/to the capture PTR(s)
; the function writes to the output destination
; the function returns and its frame is decremented

FUN size; function header, denotes frame of function should be size bytes. All closures point to such a header.
NOP; do nothing.

JMP addr; jump to addr
CAL addr; call function with header at addr
BRZ addr x; jump to addr if m[x] == 0
BRE addr x; jump to addr if m[x] != 0
BRN addr x; jump to addr if (int64_t)m[x] < 0
BRP addr x; jump to addr if (int64_t)m[x] > 0

RET; returns to the parent frame context and parent frame instruction counter.
CNT addr; overwrites parent frame retn address and prev pointer with that of the current frame, then goes into parent frame context and jumps to addr
RES addr; goes into parent frame context and jumps to addr

PRD x; prints m[x] in decimal as uint64_t
PRH x; prints m[x] in hexadecimal as uint64_t

SET x v; m[x] = v
CPY x y; m[x] = m[y]
SWP x y; swap m[x] and m[y]
NEG x y; m[x] = -m[y]
PCT x y; m[x] = popcount(m[y])

ADD x y z; x = y+z
SUB x y z; x = y-z
MUL x y z; x = y*z
DIV x y z; x = y/z

; ptr is the offset (relative to current frame memory) of a pointer.
PTRREAD ptr x s; memcpy(&m[x], ptr, s)
PTRWRITE ptr x s; memcpy(ptr, &m[x], s)
PTRMAKE ptr x; ptr = &m[x], also increments m
*/

typedef struct {
	size_t operands[3];
	Op op;
} Instr;

typedef struct frame Frame;

// Pointers store their parent frame and an offset to aid garbage collection analysis.
// The referenced value is found at ptr.frame->mem[ptr.memOff].
typedef struct {
	Frame *frame;
	size_t memOff;
} Ptr;

// A frame is a garbage collected (reference counting) activation record on the heap, parent pointer tree.
// When a frame is collected, the list of pointers is iterated and the reference count of their target frame is decremented.
// Each function call creates a frame which is the function in which it will execute.
// Frames contain all the arguments and local variables for a function.
// Note: frames do not grow or shrink, their size is set.
// Example: to make a dynamic array, a new frame needs to be created that is sized for the new array, elements copied, old frame decremented.
struct frame {
	Frame *prev; // The frame of the caller
	Instr *retn; // Continue executing code from here after returning (saved caller instruction pointer)
	             // `resume`   will transfer control into the caller context
	             // `continue` will transfer control into the caller context and update the retn,prev of the caller frame to that of this frame
	size_t refc; // How many references exist to data held in this frame
	size_t size; // Combined size in bytes of all frame variables

	// List of pointers for GC bookkeeping purposes
	size_t nPtrs; // Like the frame size, doesn't change
	Ptr  **ptrs;  // Vars in mem that are pointers, for instance, ptrs[0] = &mem[8].

	// Memory holding the local variables
	char   mem[/* size */];
};

// A closure is an executable function optionally bundled with data.
// Closures are themselves also stored in frames.
// Arguments are stored in the called frame.
// Captures are stored in the caller frame and pointers to them are passed as arguments.
// Values being returned are realized by means of passing a pointer to the return value as an argument.
typedef struct {
	Frame *captured; // Reference counts.
	Instr *function; // The actual instructions of the closure.
} Closure;

Frame *newFrame(Frame *parent, Instr *retn, size_t size, size_t nPtrs){
	Frame *f = calloc(1, size + sizeof *f);
	*f = (Frame){
		.prev  = parent,
		.retn  = retn,
		.refc  = 1,
		.size  = size,
		.nPtrs = nPtrs,
		.ptrs  = calloc(nPtrs, sizeof *f->ptrs)
	};
	return f;
}

void debugFrame(Frame *f){
	/*
	0 fn main(){
	1 	int x = 5
	2	f(x)
	3	print(x)
	4 }
	5
	6 fn f(){
	7	x = 4
	8 }
	9

	frame 0 (main)
		prev: NULL
		retn: CLEANUP
		refc: 2
		size: 4
		====[ locals ]====
		int x = 5

	frame 1 (f)
		prev: 0
		retn: line 3
		refc: 1
		size: 4
		====[ locals ]====
		TYPE	NAME	VALUE
		int 	x   	4
	*/
}

int main(void){

	// A frame to hold the global variables
	Frame *mainframe = newFrame(NULL, NULL,);

	// Execution context
	Instr *code = NULL;
	Frame *vars = NULL;

	if (code->op == CALL){
		// handle function call instruction
		// CALL addr
		vars = newFrame(vars, code, (*(Closure*)code->x).frameSize); // Create activation record and update execution context
	}
	if (code->op == RET){
		// handle ret instruction
		// RET
		Frame *old = vars;
		vars = vars->prev; // Return to caller execution context
		code = vars->retn;
		refDec(old); // Decrease refcount of old frame, if no references have been created it will be cleaned up right away.
	}
}
