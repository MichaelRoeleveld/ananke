#include "bytereader/bytereader.h"
#include "table.c"
#include "regex.h"
#include <string.h>

typedef enum {
	tinvalid, tnum, tident, tbreak, tredo, tskip, tfall, tgoto, tret, tdefer, tcheck, tassume, tresume, tcontinue, tlabel, tcase, tfn, tif, twhile, tfor, tuntil, tthen, tof, tlet, tvar, tdollar, tbackslash, tlparen, trparen, tlbrack, trbrack, tlbrace, trbrace, tminus, tplus, teq,
} Token;

typedef struct {
	Token token;
	union {
		char *name;
		void *number;
	};
} Lexeme;

struct {
	Token token;
	char *pattern;
	regex_t *regex;
} trs[]  = {
	{tfor, "^for\\b"},
	{tuntil, "^until\\b"},

	{tnum, "^([0-9]+)"},
	{tident, "^\\s*([a-z_][a-z0-9_]*)\\b"},
};
const size_t ntrs = sizeof(trs)/sizeof(trs[0]);

regex_t* compileRegex(char *pattern){
	regex_t* regex = malloc(sizeof(regex_t));
	if (regcomp(regex, pattern, REG_EXTENDED|REG_ICASE) != 0){
		fprintf(stderr, "Error compiling regex: %s\n", pattern);
		free(regex);
		return NULL;
	}
	return regex;
}

void compileAllRegex(void){
	for (size_t i=0; i<ntrs; i++){
		trs[i].regex = compileRegex(trs[i].pattern);
	}
}

void freeRegex(regex_t* regex){
	regfree(regex);
	free(regex);
}

void matchFristRegex(char *str){
	regmatch_t match[2]; // [0] is whole, [1] is group
	for (int i=0; i<ntrs; i++){
		if (regexec(trs[i].regex, str, 2, match, 0) == 0){
			int s = match[0].rm_so;
			int e = match[0].rm_eo;
			printf("matched %d /%s/ '%.*s'\n", i, trs[i].pattern, e-s, str);
			if (match[1].rm_so != -1){
				// capture group
				int start = match[1].rm_so;
				int end = match[1].rm_eo;
				printf("\tcaptured: '%.*s'\n", end-start, str+start);
				return;
			}
		}
	}
}

bool strEq(const void *k1, const void *k2){
	return 0==strcmp((const char*)k1, (const char*)k2);
}

Hash hash(const void *k) {
	// Algorithm: FNV-1a 64 bit
	const uint64_t FNV_OFFSET_BASIS = 14695981039346656037ull;
	const uint64_t FNV_PRIME = 1099511628211ull;
	Hash h = FNV_OFFSET_BASIS;
	for (size_t i=0; i<strlen((const char*)k); i++){
		h ^= ((unsigned char*)k)[i];
		h *= FNV_PRIME;
	}
	return h+1; // +1 to prevent accidentally outputting a tombstone
}

int main(void){
	Table t = tableNew(strEq, hash, NULL, NULL);
	tableSet(&t, "hello", &(int){4});
	tableSet(&t, "world", &(int){7});
	tableRem(&t, "hello");
	int *v;
	if ((v=tableGet(&t, "world")) != NULL){
		printf("w = %d\n", *v);
		// the value for "world" is *v
	} else {
		// "world" is not in the table
	}
	tableDel(&t);

	compileAllRegex();
	matchFristRegex("000A");
}
