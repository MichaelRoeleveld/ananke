#include <stdio.h>
#include <stdlib.h>

// Dynamic integer type
typedef struct {
	int first; // most significant
	int nRest;
	unsigned *rest; // as index increases, significance decreases
} bint;

void addcarry(unsigned *sum, unsigned *carry, unsigned a, unsigned b){
	// perform sum = a+b+carry
	// set carry to 1 if this expression overflows
	// set it to 0 otherwise
}

// Add two dynamic ints into dst
bint addint(bint a, bint b){
	bint ret = {0};
	ret.nRest = a.nRest;
	if (b.nRest > ret.nRest) ret.nRest = b.nRest;
	ret.rest = calloc(sizeof(ret.rest[0]), ret.nRest);

	unsigned carry = 0;
	unsigned sum = 0;
	for (int i=0; i<ret.nRest; i++){
		unsigned ai = i>=a.nRest ? a.rest[i] : 0;
		unsigned bi = i>=b.nRest ? b.rest[i] : 0;
		if (__builtin_add_overflow(ai, bi, &sum)) {
		    printf("Overflow occurred\n");
		    carry = 1;
		} else {
		    printf("No overflow\n");
		    carry = 0;
		}
		ret.rest[i] = sum;
	}
}

// Dynamic fractional type
typedef struct {
	bint numer;
	bint denom;
} brat;

int main(void){

}