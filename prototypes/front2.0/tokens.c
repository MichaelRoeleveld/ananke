#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <gmp.h>

#include "prelude.h"

#define KW(x) X(tk##x, #x)
#define KEYWORDS \
	KW(let), \
	KW(var), \
	KW(any), \
	KW(pic), \
	KW(fn), \
	KW(str), \
	KW(int), \
	KW(nat), \
	KW(rat), \
	KW(bool), \
	KW(void), \
	KW(where), \
 \
	KW(is), \
	KW(_), \
	KW(this), \
	KW(self), \
	KW(it), \
	KW(now), \
	KW(nil), \
 \
	KW(case), \
	KW(of), \
	KW(if), \
	KW(elif), \
	KW(else), \
	KW(then), \
 \
	KW(while), \
	KW(until), \
	KW(for), \
	KW(in), \
	KW(goto), \
	KW(destination), \
	KW(ret), \
	KW(break), \
	KW(skip), \
	KW(next), \
	KW(redo), \
	KW(fall), \
	KW(continue), \
	KW(resume), \
 \
	KW(and), \
	KW(or), \
	KW(not), \
	KW(xor), \
	KW(true), \
	KW(false) \

#define SYMBOLS \
	X(tkcmp, "<=>"), \
	X(tkddot, ".."), \
	X(tkpplus, "++"), \
	X(tkmmin, "--"), \
	X(tkppct, "%%"), \
	X(tkhhuh, "??"), \
	X(tkllt, "<<"), \
	X(tkggt, ">>"), \
	X(tkeqq, "=="), \
	X(tkge, ">="), \
	X(tkle, "<="), \
	X(tkeq, "="), \
	X(tklt, ">"), \
	X(tkgt, "<"), \
	X(tkls, "["), \
	X(tklp, "("), \
	X(tklc, "{"), \
	X(tkrs, "]"), \
	X(tkrp, ")"), \
	X(tkrc, "}"), \
	X(tklm, "\\"), \
	X(tkstar, "*"), \
	X(tkslash, "/"), \
	X(tkplus, "+"), \
	X(tkmin, "-"), \
	X(tkamp, "&"), \
	X(tkcar, "^"), \
	X(tkpct, "%"), \
	X(tkdol, "$"), \
	X(tkhash, "#"), \
	X(tkat, "@"), \
	X(tkexc, "!"), \
	X(tktilde, "~"), \
	X(tkbktick, "`"), \
	X(tkpipe, "|"), \
	X(tkcol, ":"), \
	X(tkdot, "."), \
	X(tkcomma, ","), \
	X(tkhuh, "?") \

#define TOKENS X(tkeof, "<end of input>"),\
	KEYWORDS, SYMBOLS,\
	X(tkid, "<identifier>"),\
	X(tkstrlit, "<string literal>"),\
	X(tknumlit, "<numeric literal>")

#define X(e, s) s
static char *strs[] = {TOKENS};
#undef X
#define X(e,s) e
typedef enum {TOKENS, ntokens} Token;
#undef X

typedef struct {
	Token tok;
	unsigned short line;
	unsigned short col;
	union {
		unsigned str;
		Rat rat;
	};
} Lxm;

void lxm_dbg(Lxm x){
	fprintf(stderr, "LEXEME:\n");
	fprintf(stderr, "  tok: '%s'\n", strs[x.tok]);
	if (x.tok == tknumlit)
		fprintf(stderr, "  rat:  %lu/%lu\n", x.rat.num, x.rat.denom);
	else if (x.tok == tkstrlit || x.tok == tkid)
		fprintf(stderr, "  str:  %u (%s)\n",  x.str, strings_getstr(x.str));
	fprintf(stderr, "  line: %hu\n", x.line);
	fprintf(stderr, "  col:  %hu\n\n", x.col);
}

static struct {
	Queue token_queue;
	char *linestart;
	char *input;
	unsigned line;
} lexer;

void lex_init(char *src){
	lexer.line = 1;
	lexer.input = src;
	lexer.linestart = lexer.input;
	queue_init(&lexer.token_queue, sizeof(Lxm), 0);
}

static inline
void crash(char *why){
	fprintf(stderr, "%s\n", why);
	exit(1);
}


static inline
int eat_char(void){
	if (!*lexer.input){
		return 0;
	}
	if (*lexer.input == '\n'){
		lexer.line++;
		lexer.linestart=lexer.input+1;
	}
	++lexer.input;
	return 1;
}

static inline
int match(char *needle){
	if (strstr(lexer.input, needle) == lexer.input) {
		lexer.input += strlen(needle);
		return 1;
	}
	return 0;
}

// Is a string id a keyword
int is_keyword(unsigned x){
	return x >= tklet && x <= tkfalse;
}

static inline unsigned curr_col(void){ return lexer.input-lexer.linestart+1; }

void lex_dbg(void){
	fprintf(stderr, "LEXER STATE:\n");
	fprintf(stderr, "  input: '%.6s...' \n", lexer.input);
	fprintf(stderr, "  line: %u\n", lexer.line);
	fprintf(stderr, "  linestart (col): '%.6s...' %u\n", lexer.linestart, curr_col());
	fprintf(stderr, "  currch: '%c'\n\n", *lexer.input);
}


// Eat a lexeme from the input stream and return it
Lxm lex_scan(void){
	if (!lexer.input){
		crash("LEXICAL ERROR: attempt to advance a NULL input");
	}
top:

	// Skip whitespace
	if (isspace(lexer.input[0])){
		eat_char();
		goto top;
	}

	// Unprintables (newlines are unprintable so got rid of those first)
	if (!isprint(lexer.input[0])){
		if (!lexer.input[0]){
			return (Lxm){.tok=tkeof, .line=lexer.line, .col=curr_col()};
		}
		crash("LEXICAL ERROR: Invalid character");
	}
	
	// Skip multiline comments
	if (match("/.")){
		int lvl = 1; do {
			if (match("/.")){
				lvl++;
			} else if (match("./")){
				lvl--;
			} else if (!eat_char()){
				crash("LEXICAL ERROR: Unterminated comment");
			}
		} while (lvl > 0);
		goto top;
	}


	unsigned line = lexer.line;
	unsigned col = curr_col();
	// Identifiers
	if (isalpha(lexer.input[0]) || lexer.input[0] == '_'){
		char *word = lexer.input;
		do {
			eat_char();
		} while (isalnum(lexer.input[0]) || lexer.input[0] == '_');
		size_t len = lexer.input-word;
		unsigned str = strings_getid(len, word);
		Token tok = is_keyword(str)?str:tkid;
		return (Lxm){.tok=tok, .line=line, .col=col, .str=str};
	}

	// Numbers
	if (isdigit(lexer.input[0])){
		char *start = lexer.input;
		u64 num = 0;
		u64 denom = 1;
		bool overflow = 0;
		do {
			overflow |= num > UINT64_MAX/10;
			num *= 10;
			num += lexer.input[0]-'0';
			eat_char();
			while (lexer.input[0] == '_') eat_char();
		} while (isdigit(lexer.input[0]));


		if (lexer.input[0] == '.' && isdigit(lexer.input[1])){
			eat_char(); // .
			do {
				overflow |= num > UINT64_MAX/10;
				overflow |= denom > UINT64_MAX/10;
				denom *= 10;
				num *= 10;
				num += *lexer.input-'0';
				eat_char(); // digit
				while (lexer.input[0] == '_') eat_char();
			} while (isdigit(lexer.input[0]));
		}

		if (overflow){
			fprintf(stderr, "Numeric literal '%*s' can not be stored. Numerator and/or denominator does not fit in 64 bits.\n", (int)(lexer.input-start), start);
			exit(1);
		} else {
			return (Lxm){.tok=tknumlit, .line=line, .col=col, .rat=(Rat){num, denom}};
		}
	}

	// Symbols
	for (size_t i=tkcmp; i<=tkhuh; i++) {
		if (match(strs[i])){
			return (Lxm){.tok=i, .line=line, .col=col};
		}
	}
	goto top;
}

// Advance the lexer by popping from the queue (or if empty, scanning the next lexeme)
Lxm lex_adv(){
	if (queue_is_empty(&lexer.token_queue)){
		return lex_scan();
	}
	return *(Lxm*)queue_pop(&lexer.token_queue);
}

// Peek the next token, or the current token if ahead=0
Lxm lex_peek(unsigned ahead){
	while (ahead >= queue_count(&lexer.token_queue)){
			Lxm tmp = lex_scan();
			queue_push(&lexer.token_queue, &tmp);
	}
	Lxm *lxm = queue_peek_n(&lexer.token_queue, ahead);
	assert(lxm);
	return *lxm;
}

int main(){
	strings_init();
	for (size_t i=tklet; i<=tkfalse; i++){
		unsigned x = strings_getid(strlen(strs[i]), strs[i]);
		if (x != i){printf("exp/got: %lu/%u\n", i, x); exit(1);}
	}

	lex_init("if \nyou?\n12\n3.14 69 420 9_.2");
	puts("Peek 1 ahead:");
	lxm_dbg(lex_peek(1));
	puts("");

	puts("iterate");
	Lxm lxm; do {
		lxm = lex_adv();
		lxm_dbg(lxm);
	} while (lxm.tok != tkeof);
}
