#pragma once

#define _GNU_SOURCE

#include <math.h>
#include <stdio.h>
#include <ctype.h>
#include <sched.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/sysinfo.h>

#include "rat.h"

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;

typedef long double f80;
typedef double f64;
typedef float f32;

typedef struct {
    void* data;         // Data pointer
    size_t n;           // Number of elements
    size_t capacity;    // Number of allocated slots
    size_t esize;       // Size of an element
} Arr;

void  arr_init(Arr* a, size_t element_size, size_t initial_capacity);
void  arr_insert(Arr *a, void *e);
void  arr_free(Arr* a);
void *arr_get(Arr *a, size_t i);
void  arr_grow(Arr *a);


typedef struct {
	Arr arr;
	size_t read, write;
} Queue;

void   queue_init(Queue *q, size_t elem_size, size_t initial_capacity);
void   queue_push(Queue *q, void *elem);
void   queue_free(Queue *q);
void   queue_dbg(Queue *q);
void  *queue_pop(Queue *q);
int    queue_is_empty(Queue *q);
void  *queue_peek(Queue *q);
size_t queue_count(Queue *q);
void  *queue_peek_n(Queue *q, size_t n);


void  strings_dbg(void); // debug the string table
void  strings_init(void); // run before using
void  strings_free(void); // run before exit
char *const strings_getstr(u32 id); // turn an id into a string WARNING reuses an internal buffer. Use strdup to "keep".
u32   strings_getid(size_t len, const char str[len+1]); // turn a string into an id. len is the string length, so exclude the 0 byte

void arr_init(Arr* a, size_t element_size, size_t initial_capacity) {
	if (initial_capacity < 1) initial_capacity = 1;
    a->n = 0;
    a->capacity = initial_capacity;
    a->esize = element_size;
    a->data = calloc(initial_capacity, element_size);
    assert(a->data != NULL);
}

void arr_grow(Arr *a){
	a->capacity *= 2;
	a->data = reallocarray(a->data, a->capacity, a->esize);
	if (a->data == NULL) {
		// Handle memory reallocation failure
		exit(EXIT_FAILURE);
	}
}

void arr_insert(Arr *a, void *e) {
	if (a->n == a->capacity) {
		arr_grow(a);
	}
	memcpy((char*)a->data + a->n * a->esize, e, a->esize);
	a->n++;
}

void *arr_get(Arr *a, size_t i) {
	if (i >= a->n) {
		return NULL;
	}
	size_t byte_offset = a->esize*i;
	unsigned char *byte_start  = (unsigned char*)a->data + byte_offset;
	return (void*) byte_start;
}

void arr_free(Arr* a) {
	free(a->data);
	a->data = NULL;
	a->n = 0;
	a->capacity = 0;
}


void queue_init(Queue *q, size_t elem_size, size_t initial_capacity){
	if (initial_capacity < 2) initial_capacity = 2;
	arr_init(&q->arr, elem_size, initial_capacity);
	q->read = q->write = 0;
	q->arr.n = q->arr.capacity;
}

void queue_push(Queue *q, void *elem){
	size_t oldcap = q->arr.capacity;
	if ((q->write+1)%oldcap == q->read){
		arr_grow(&q->arr);
		size_t newcap = q->arr.capacity;

		if (q->write <= q->read){
			// move [R, len) to [R+x, len+x) where len+x = newlen
			size_t oldoffs = q->read;
			size_t n_elems = oldcap - oldoffs;
			// R+x = newoffs
			// newlen = len+x
			// x = newlen - len
			size_t newoffs = q->read + (newcap - oldcap);
			size_t e_size = q->arr.esize;
			unsigned char *base = q->arr.data;
			memmove(base+newoffs*e_size, base+oldoffs*e_size, n_elems*e_size);
			q->read = newoffs;
		}
		q->arr.n = q->arr.capacity;
	}
	void *dst = arr_get(&q->arr, q->write++);
	assert(dst);
	memcpy(dst, elem, q->arr.esize);
	if (q->write == q->arr.capacity) q->write = 0;
}

static inline
void blob_dbg(size_t n, unsigned char data[n]){
	for(size_t i=0; i<n; i++){
		fprintf(stderr, "%02X", data[i]);
	}
}

void queue_free(Queue *q){
	arr_free(&q->arr);
}

void queue_dbg(Queue *q){
	size_t n = q->arr.capacity;
	size_t elem_size = q->arr.esize;
	unsigned char *elems = q->arr.data;
	for (size_t i=0; i<n; i++){
		blob_dbg(elem_size, elems + i*elem_size);
		fprintf(stderr, i+1==n?"\n":", ");
	}
	// chars printed: (esize*2 + 2)*offset
	// print read head
	fprintf(stderr, "%*s^READ\n",(int)((elem_size*2+2)*q->read), "");
	// print write head
	fprintf(stderr, "%*s^WRITE\n\n",(int)((elem_size*2+2)*q->write), "");
}

void *queue_pop(Queue *q){
	if (q->read == q->write) return NULL;
	void *r = arr_get(&q->arr, q->read++);
	if (q->read == q->arr.capacity) q->read = 0;
	return r;
}

int queue_is_empty(Queue *q){
	return q->read == q->write;
}

void *queue_peek(Queue *q){
	if (queue_is_empty(q)) return NULL;
	unsigned char *base = q->arr.data;
	size_t byte_offs = q->read * q->arr.esize;
	return &base[byte_offs];
}

void queue_test(void){
	Queue qstore;
	Queue *q = &qstore;

	// test grow with w > r
	queue_init(q, sizeof(int), 4);
	queue_push(q, &(int){1});
	queue_push(q, &(int){2});
	queue_push(q, &(int){3});
	queue_push(q, &(int){4});
	assert(q->write == 4);
	assert(q->read == 0);
	assert(q->arr.capacity > 4);
	assert(queue_pop(q));
	assert(queue_pop(q));
	assert(queue_pop(q));
	assert(queue_pop(q));
	assert(!queue_pop(q));
	assert(q->write == 4);
	assert(q->read == 4);
	assert(q->arr.capacity > 4);
	queue_free(q);

	// test grow with w < r
	queue_init(q, sizeof(int), 4);
	queue_push(q, &(int){-1});
	queue_pop(q);

	queue_push(q, &(int){1});
	queue_push(q, &(int){2});
	queue_push(q, &(int){3});
	queue_push(q, &(int){4});

	int *x = queue_peek(q);
	assert(x);
	assert(*x == 1);
	int *y = queue_pop(q);
	assert(y);
	assert(*x == *y);
	x = queue_pop(q);
	assert(x && *x==2);
	x = queue_pop(q);
	assert(x && *x==3);
	x = queue_peek(q);
	assert(x && *x==4);
	x = queue_pop(q);
	assert(x && *x==4);
	assert(!queue_pop(q));
	assert(!queue_peek(q));
	queue_free(q);
}

size_t queue_count(Queue *q){
	if (q->write < q->read){
		assert(q->arr.capacity > q->read);
		return q->arr.capacity - q->read + q->write;
	}
	return q->write - q->read;
}

void *queue_peek_n(Queue *q, size_t n){
	if (n >= queue_count(q)) return NULL;
	size_t elem = (q->read + n)%q->arr.capacity;
	unsigned char *base = q->arr.data;
	return base + elem*q->arr.esize;
}

// Ternary search tree
typedef struct TST TST;
struct TST {
    TST *lt, *eq, *gt;
    u32 id;
    char x;
};

// Private global variables
static TST* globalRoot = NULL;
static int autoIncrement = 1;

static size_t nodect = 0;

// Function to create a new TST node
static inline
TST* newNode(char x) {
    nodect++;
    TST* node = malloc(sizeof(TST));
    node->x = x;
    node->id = 0;
    node->lt = node->eq = node->gt = NULL;
    return node;
}

// Function to insert or retrieve a str-id pair in the TST
static inline
int str2id(size_t n, const char str[n+1]){
    TST **root = &globalRoot;
    TST *node;

    while(1){
        if (!(*root)) {
            *root = newNode(*str);
        }

        node = *root;
        char x = node->x;
        
        if (*str < x) {
            root = &node->lt;
        } else if (*str > x) {
            root = &node->gt;
        } else if (n > 1) {
            // Continue to the next character
            root = &node->eq;
            n--;
            str++;
        } else {
            // String already exists, return existing id
            if (node->id == 0) {
                // If id is not assigned, assign an auto-incremented id
                node->id = autoIncrement++;
            }
            break;
        }
    }
    return node->id;
}

static inline
int id2str(TST *t, char *buf, int id){
    if (t) {
        if (t->id == id){
            buf[1] = '\0';
            buf[0] = t->x;
            return 1;
        }

        if (id2str(t->eq, buf+1, id)){
            buf[0] = t->x;
            return 1;
        }

        if (id2str(t->lt, buf, id) || id2str(t->gt, buf, id)){
            return 1;
        }
    }

    return 0;
}


static inline
void fhelp(TST *t){
    if (!t) return;
    fhelp(t->eq);
    fhelp(t->lt);
    fhelp(t->gt);
    free(t);
}


int maxd = 0;
int neq = 0;
int ngt = 0;
int nlt = 0;

static inline
void phelp(TST *t, int d){
    maxd = d>maxd?d:maxd;
    if (t->id){
        printf("p%p [label=\"'%c' (%u)\", color=blue]\n", t, t->x, t->id);
    } else {
        printf("p%p [label=\"'%c'\"]\n", t, t->x);
    }

    if (t->eq){
        intptr_t dist = (intptr_t)(char*)t->eq-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"= (%ld bytes away)\", color=blue]\n", t, t->eq, dist);
        phelp(t->eq, d+1);
        neq++;
    }
    if (t->lt){
        intptr_t dist = (intptr_t)(char*)t->lt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"< (%ld bytes away)\"]\n", t, t->lt, dist);
        phelp(t->lt, d+1);
        nlt++;
    }
    if (t->gt){
        intptr_t dist = (intptr_t)(char*)t->gt-(intptr_t)(char*)t;
        printf("p%p -> p%p [label=\"> (%ld bytes away)\"]\n", t, t->gt, dist);
        phelp(t->gt, d+1);
        ngt++;
    }
}

// Public API
// void debugStrings(){
//     puts("digraph g {");
//     phelp(globalRoot,1);
//     printf("label=\"%lu nodes of %lu bytes each = %lu bytes.\nMaximum depth: %d. Edge counts: (%d <) (%d =) (%d >)\"\n",nodect, sizeof(TST), nodect*sizeof(TST), maxd, nlt, neq, ngt);
//     puts("}");
// }

void strings_init(){}

void strings_free(){
    fhelp(globalRoot);
}

char *const strings_getstr(u32 id){
    static char buf[1024] = {0};
    if (id && id2str(globalRoot, buf, id)){
        return buf;
    } else {
        return NULL;
    }
}

u32 strings_getid(size_t len, const char str[len+1]){
    assert(len < 1024);
    return str2id(len, str);
}
