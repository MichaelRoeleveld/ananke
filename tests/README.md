# Automated testing

This is a repository of end to end tests.

[[_TOC_]]

## Goals

- Catch performance regressions
- Ensure all cases compile (or not!)
- Ensure all cases run
- Ensure all cases behave as expected

## Protocol

Building:

For each directory in `./cases`
- abort if it contains both a `main.ank` and `main.asm`
- all `main.ank` are compiled with `../bin/ank -p out.parse 2>out.log > out.asm`
- - stdout is piped into `out.asm`
- - stderr is piped into `out.log`
- - The parse tree is put in `out.parse`
- all `main.asm` and `out.asm` and assembled with `../bin/asm 2>>out.log > out.avm`
- - stdout is piped into `out.avm`
- - stderr is pipe-appended into `out.log`


Executing:

For each directory in `./cases` that contains `out.avm`
- `out.avm` is executed with `../bin/avm out.avm $(cat in.args) < in.in 2> out.err > out.out`
- - `in.args` passed as argument vector
- - `in.in` piped into `stdin`
- - `stdout` and `stderr` are piped to `out.out` and `out.err`.
- if there is an `exp.time`, alert if `out.avm` runs longer and abort that VM
- - `timeout -k 1 $(cat exp.time) $COMMAND || echo "$CASE timed out"`

Checking:

For each directory in `./cases`
- For each file starting with `exp.` except `exp.time`
- - Alert if there is no corresponding `out.` file
- - Alert if the file contents of `exp.` and `out.` are different

Clearing:

- delete all `out.*` in all case directories.

## Basic file structure

```
cases
|
|- one_case
|  |
|  |- main.asm
|  |
|  |- in.args     (optional)
|  |- in.in       (optional)
|  |
|  |- exp.time    (optional)
|  |- exp.out     (optional)
|  '- exp.err     (optional)
|
'- another_case
   |
   |- main.ank
   |
   |- in.args     (optional)
   |- in.in       (optional)
   |
   |- exp.parse   (optional)
   |- exp.time    (optional)
   |- exp.avm     (optional)
   |- exp.log     (optional)
   |- exp.out     (optional)
   |- exp.err     (optional)
   |
   |- out.parse   (generated)
   |- out.avm     (generated)
   |- out.log     (generated)
   |- out.out     (generated)
   '- out.err     (generated)
```

## Commands

Build, execute, check
```
./run.py
```

Clear artifacts and test results
```
./run.py clean
```

Build artifacts
```
./run.py build
```

Execute artifacts (checks time)
```
./run.py exec
```

Run checks (does exp/out comparison)
```
./run.py check
```
