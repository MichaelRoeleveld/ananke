// parametric grammar: rule{param}
list{x,sep,n} // a list of at least n xes seperated by seps
csl{x,n} = list{x,',',n}
csl{x} = csl{x,0}
csl1{x} = csl{x,1}

// regex
name = /[a-zA-Z_][a-zA-Z0-9_]*/
number = hex | bin | dec
hex = /0x[0-9a-fA-F]+\.[0-9a-fA-F]+/
bin = /0b[01]+\.[01]+/
dec = /[0-9]+\.[0-9]+/

body = '{' program '}'
matchbody = '{' csl{ exp ':' exp } '}'
array = '[' csl{exp} ']'
tuple = '(' ')'

// rules are tried in order and matched greedily
nud = 
	'(' exp ')'
	body
	array
	'break'  name?
	'redo'   name?
	'skip'   name?
	'fall'   name?
	'fork'   exp
	'join'   exp
	'goto'   exp
	'ret'    exp
	'eval'   exp
	'defer'  exp
	'check'  exp
	'assume' exp
	'resume'
	'continue'
	'label'  name exp
	'case'   exp  matchbody
	'fn'     name? '(' csl{decl} ')' body
	'\'      csl1{name} exp
	'\\'     exp
	'if'     exp exp ('elif' exp exp)* ('else' exp)?
	'while'  exp exp
	'for'    csl1{name} 'in' exp exp
	'-'      exp
	decl     name
	name
	number

decl = '$' | typename
type = 'let'|'var'| <<context>>

led{0} // priority 0
	'(' csl{exp} ')'
	'[' csl{exp} ']'

led{1}
	'=' exp

led{4}
	'+' exp
	'-' exp
	'until' exp
	'then'  exp ('else' exp)?
	'of'    matchbody


led{1} // prio 1


exp = exp{0}
exp{n} = nud led{n}*

program = exp{0}*