# feel free to open an issue and weigh in on this!

Context:
* Ananke is a statically, strongly typed language with local type inference and global static duck typing. Types are noted right to left, meaning a variable `x` which is pointer to int is noted `i32* x`.
* Ananke has no semicolons.
* Ananke has UCS, meaning `fun(thing) == thing.fun()`
* Ananke allows user defined (`/[A-z][A-z0-9_]*/`) operators, such as `x dot y`

# ghost function call

`expr(expr);` or `expr; (expr);`?

	var x = 6
	var y = x
	(3 to str).printl()

* solution A: context dependant parsing  
* problem A: does not solve function pointers
```
let p = printl
(3 to str).p()
```
* solution B: programmer uses {} instead  
* problem B: unintuitive
```
let p = printl
{3 to str}.p()
```
* solution C: significant leading whitespace for parentheses via a special `(` token (`t_lparen_space`) which acts the same as `t_lparen` except a function call can only be made with a `t_lparen`.  
* problem C: some programmers may prefer to start a function call parenthesis on a new line
```
bigfun
(
	some_arg,
	othr_arg,
	this_arg,
	that_arg,
	many_arg
)
```
## ghost array access

The same problem, really.

	let x = y
	[1,2,3].map(fn(x){printl(x to str)})

Even more niche as I predict very few useful expressions would lead with a `[`.

# array of fn, or fn returning array?

	fn(i32)i32[] f = [fn(i32 x)i32{x}]

Apparently supposed to be an array!

* solution: `()` around parts of types
* problem: introduces other ambiguities
```
(fn(i32)i32)[] f = maybe ghost function call
```
* solution 2: left to right types
* problem 2: introduces other ambiguities
```
i32 x = y
[]fn(i32)i32 f = maybe ghost array access
```
* solution 3: return type in front of argument type
* problem 3: introduces ambiguity with unnamed/nothing-returning functions
```
fn f (i32) {}
/. is f the return type, or the function name? ./
```
* solution 4: move the return type ALL THE WAY up front
* problem 4: just look at it
```
i32 fn myfun(i32){}
```
* solution 5: swap return type and name
* problem 5: looks unintuitive, does not allow tail-call recursive parsing
```
fn i32(i32) myfun{}
```
# operator or func?

	op (lhs) dot (rhs) {}
	x dot (y dot z)

* solution: context dependent parsing  
* solution 2: programmer uses {} instead of ()

# shift or template?

	type thing<type T>{}

	thing<thing<thing<i32>>>

* problem: is that a `>>>` operator or 3 `>`s?
* solution: accept not just `>` but also `>>` and `>>>` as template parameter closing braces. Requires context.
```
if we are in a template parameter parse:
	if the current token has a `>` in it:
		act as if we saw a "pure `>`" token
		remove a single `>` from the actual token
```
* problem 2:
```
type thing2<i32 n>{}
thing< thing2< 3>>5 > >
```
* solution 2: programmer uses `()`
```
thing< thing2< (3>>5) > >
```
* solution 3: template parameters use `[]` instead of `<>`  
* problem 3.1: what about closure lexical captures then, what will they use?
* problem 3.2: introduces ambiguity around arrays, will likely mess with the static ducktyping type inference engine
