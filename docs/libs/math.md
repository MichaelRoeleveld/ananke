functions

	div(x,y)
		returns both quotient and remainder
	cmod(x,y)
		c % operator behavior
	abs(x)
		returns unsigned version of x
	sign(x)
		x/abs(x)
	abs(vec)
		returns the hypotenuse (if 2d) aka magnitude (euclidian distance between vec and origin aka lenght)
	
	sin(x)
	cos(x)
	tan(x)
	asin(x)
	acos(x)
	atan(x)
	qatan(x,y)
		atan, but accounts for quadrants, range is 0-tau
		angle([x,y])
	
	sinh
	cosh
	tanh
	asinh
	acosh
	atanh

	ceil(x) == round up to positive infinity
	floor(x) == round down to negative infinity
	round(x) == floor(x+0.5)
	
	avg(a)
	median(a)
	mode(a)
	stdev()
	nCr(n,r)
	nPr(n,r)
	err(x)
		https://en.m.wikipedia.org/wiki/Error_function
	gamma(x)
		https://en.m.wikipedia.org/wiki/Gamma_function
	fact(x)
		gamma(x+1)

	sqrt(x)
		x^0.5
	cbrt(x)
		x^(1/3)
	exp(x)
		e^x
	ln(n)
		https://en.m.wikipedia.org/wiki/Natural_logarithm
	log(base, n)
		ln(n)/ln(base)
	lg2(n)
		log(2,n)
	lg10(n)
		log(10,n)
	
note the following functions present in most other languages are not implemented:

	pow
		just do b^x
	max, min
		just do [a,b,...?].max
	rem
		just do a%b

constants

	pi = 3.14159
	tau = 2*pi
	e = 2.71828
	i = sqrt(-1)
	phi = 1.61803
	true = 1
	false = 0