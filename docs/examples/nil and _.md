# Nothing and Anything
Ananke has 2 most special "language constants", `nil` and `_`, or _nothing_ and _anything_ in plain English.

## nil (nothing)

`nil` is used to signify there is no value. How many plies of toilet paper are on a roll? If the answer is 0, there is no toilet paper on the roll. If it were `nil`, then there is no roll at all.

We can make a sum type with `nil` easily:

	i32? plies1 = nil
	i32? plies2 = 0
	i32? plies3 = 45
	; the `?` in the context of a type expression means "...maybe"
	; i32... maybe (or maybe nil)

*In a type,* the `?` applies to everything to the left of it:

	i32[]? a = nil
	i32[]? b = [1,2,3]
	; an array of i32, or nil
	; this tells us there may not be an array

	i32?[] c = [9,nil,1]
	i32?[] d = [4,5,6]
	; an array of maybe-i32
	; (each element is an i32 or nil)
	; this tells us there is always an array, but some elements may not exist

	i32?[]? e = nil
	i32?[]? f = [1,nil]
	i32?[]? g = [1,2]
	; a maybe-array of maybe-i32
	; this tells us there can be no array, or an array where some elements may not exist.

The type of `nil` is `void`. So `i32?` is a shorthand for `type{i32/void}`.

### nil-safe operator (?)

In a normal expression, the `?` acts as a nil-safe operator:

	fn f(x){
		printl("x is \{x}")
		return 1
	}

	i32? a = 3
	i32? b = nil
	
	let r = a?.f()
	; f is called on a because it is not nil and f returns 1
	; so r is 1.

	let s = b?.f()
	; b is nil, the `?` operator prevents f from being called.
	; as a result, s is nil (the value of b).

	check r == 1 and s == nil

This allows you to use and design functions which do not deal with maybe-types even on thing which may be nil. You can then deal (or not) with the nil at a later time.

## _ (anything)

`_` is used to signify "could be any value".

	i32 x = _; leave x unitialized
	; we are saying x might be any value at this point

It is also useful when matching. We can bind our match to a variable, but if we don't want that we can use `_`:

	i32 y = 44
	printl("y is: " .. case y {
		x > 100: "something >100, namely \{x}",
		_ > 3: "anything greater than 3",
		_: "Anything [which hasn't yet matched any of the above cases]"
	})
	; will print 'anything greater than 3'

`_` has no type because the whole idea of types is to restrict the possible values of a variable: `_` is unbounded, unrestricted, means literally anything. It *can not have* a type.