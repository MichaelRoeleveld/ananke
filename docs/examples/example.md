## Ananke has no semicolons
By carefully structuring the grammar of Ananke, semicolons could be obviated entirely. (Well, ending a line on an identifier an starting the next line with `-` or `(` can be ambiguous. In practice not a problem.)

    /. this is a comment /. nested comments are supported ./ ./

    ; create the main function
    ; it will print hello world

    fn main() {
        printl("Hello world!")
    }

You can still use them, but they will be parsed as a space.

## Ananke has only expressions
The arbitrary divide between statements and expressions is exactly that: arbitrary. Ananke allows you to do stuff like this:

    ; x is a variable and can be changed
    var x = if 3 > 5 {
        10
    } else {
        -10 
        ; of course, 5 is bigger than 3, so x will be -10
    }

    ; increment x 12 times
    do 12 { x += 1 }
    ; x will be x+1+1+1+1 ... -> -10+12 -> 2
    
    ; y is a constant and can not be changed
    let y = x
    y = 5; error!

## Ananke has compile time evaluation
Sometimes you have things you could compute in advance, so that the compiled program runs as fast as possible.

    fn compute_pi(){           ; compute pi using Taylor Series
        return 1.0 ... 100.0   ; the array [1,2,3,4,5...100]
            .map(\x x*2-1) ; make it [1,3,5,7...199]
            .fold(fn(sum, x){  ; also known as reduce()
                let sign = ((x+1)/2%2 == 1)*2-1
                               ; 1 or 0, *2-1 -> -1 or 1
                sign/x + sum   ; last expression is return value
                               ; effect: 1/1 -1/3 +1/5 -1/7 +...
            }) * 4;            ; the sum is pi/4, so *4 to get pi
    }

    let pi = eval compute_pi()
    /. the computation for pi is evaluated before execution time ./

## Ananke is statically ducktyped
you may have noticed that no type annotations, like `int` or `float` have been given thus far. That's because Ananke is ducktyped: it means as long as the variable supports the operations you want to do on it, such as the `+` operator, it will just work. If the passed argument does not support `+`, the compiler will tell you at compile time.

If you must, you can still annotate. Note how Ananke mirrors C style type annotations, where the type goes before the identifier.

    fn sqrt(f32 number) f32 {} 
    /. now we are sure sqrt takes a 32 bit floating point number and returns a 32 bit floating point number ./

## Ananke allows easy, generic data structure definitions
despite being structurally typed, sometimes you just need to invent your own explicit types, such as a binary tree.

# Note: type declaration syntax is WIP

    type<T> tree =
                 | T leaf
                 | [2]*tree<T> branch 

    /. a tree of T is either a T called leaf 
       or a 2d vector of pointers to tree of T called branch ./
    
    let leaf1 = tree{.leaf = 3.14}
    let leaf2 = tree{.leaf = 2.71}
    /. this type of assignment is called "compound literal" and replaces simple constructors ./
    /. the type parameter is inferred as f32 ./
    
    /. get the pointers to the leafnodes, and assign those as members of the branch array ./
    let rootnode = tree{.branch=[$leaf1, $leaf2]}

By the way, "tree" is what is called a sum type/enum type/variant record. Check the documentation "sumtypes.md" for more usage and pattern matching syntax.

## ranged types bring safety and allow more optimizations
The predicate is the expression after `where` which must return 1 to signify a valid object. If an object is deemed invalid, the program shall hard crash. You can test an object for validity with the `your_type._check(an_obj)` reflection.

    type even = i32 where this%2==0
    /. the type "even" is an i32 on the condition that it divides 2 ./

    /. yes, even this "even" works! ./
    type<T> generic_even = T where this%2==0
    generic_even x = 4.0

    /. this will crash with the error message "type system predicate violation" ./
    generic_even y = 3.0 

    /. it is up to the programmer to first verify the object can fit the type, for instance using the (read only) _predicate() reflection ./
    y = if generic_even._check(3.0) {
        /. code will not reach here ./
        3.0
    } else {
        some_default_value
    }

## Ananke allows data integrity at any stage of an object
Ananke has constructors, destructors.

    type mytype = i32

    construct(*mytype this){
        this$ = 0
        /. dereference the pointer and set the object to 0 ./
    }

    /. the constuctor is always called before a compound literal which means that a compound literal can overwrite some of the constructor's defaults ./

    mytype x = mytype{1} /. will be 1 ./

    destruct(*mytype this){
        /. clean this up ./
    }

They are completely optional, and mostly useful in deep datastructures like linked lists, where deletion of the head node should traverse and delete the entire list. In that case, only defining the destructor is a good design choice.

## Ananke allows exact data layouts
What if you want to network your code? You will need to send packets. What do the clusters of information look like?

    /. exact lays out the data exactly as specified, no automatic padding or reordering ./
    /. lite means little endian order is forced at storage level, unnoticed at program level ./
    exact type mypacket =
        lite [3]f32 position,
        /. 3d vector of f32s ./

        lite i32 health,
        /. 32 bit signed 2's complement integer ./
        
        u1 isAdmin,
        /. u1 = a single bit ./
        
        u7 padding
        /. 7 bits of padding ./

This is equally useful for binary file format parsing. You can directly cast an array of octets (u8) to whatever header format you desire. A caveat is data alignment: some cpus can't read unaligned members. For reading and writing to unaligned members, Ananke may insert data wrangling code where necessary to assist these poor, incapable cpus.

By the way, "mypacket" is called a product type, aka struct, kind of akin to a class.

### type conversion

you can do a converting cast with `x to y` or an interpreting cast with `x as y`.

    3.6 to i32 /. will truncate 3.6 to produce 3 ./
    3.6 as u32 /. will interpret the bits in the f32(3.6) as if it were u32 ./

## Ananke has uniform call syntax

You can either do `function(object)` or `object.function()`. And you only need to define `function` once.

## Ananke has very easy multithreading
async is colorless in Ananke, because async is declared at the call site, not the function definition.

    /. do an expensive calculation asynchronously ./
    let x = async expensive_calculation()

    /. maybe some code in between here ./

    let y = 1+x /. x is automatically awaited ./

    /. you can still manually await results ./
    let z = async expensive_calculation()
    await z 

Essentially it will feel like fork/join multithreading, but the implementation may or may not hinge on OS threads and actual forking/joining. It's up to the compiler.

locks are also center features. Use the atom keyword to make a piece of data or code atomic, that means, guarded with locks.

    atom x = 4
    /. x can be safely accessed and edited from multiple threads ./

    atom fn critical(){
        /. this function called critical will only have one running instance at a time in the whole program. hence, a critical section. ./
    }

    atom {
        /. this is an atomic code block. Also acts as a critical section, one thread at a time may execute it ./
    }

In the works are compare-and-exchange, test-and-set operations.

## Ananke has modules
Say no to `#include`s! Say yes to `use`!

    module mymod {
        private fn helperfunc(){};
        fn somefunc(){}
    }

In another file:

    use mymod
    mymod.somefunc()
    /. or, to merge the module namespace with the current scope: ./
    use mymod.*
    somefunc()

## Ananke facilitates interoperability
Want to interface with C? or perhaps another language the compiler understands?

# Note this syntax is WIP

    import "language" "symbolname" in "libname" as ananketype

    module pthread {
        type pthread = import "c" "pthread_t"      in "pthread" as i32
        type attr    = import "c" "pthread_attr_t" in "pthread" as i32
        /. I'm not actually sure what the underlying type is, pthread.h is an opaque header ./
        let create   = import "c" pthread_create"  in "pthread" as fn(*pthread, *attr, *fn(uptr)uptr, uptr) 
        /. where just "uptr" is void* ./
    }

Note below example is probably incorrect but allows for a "feel" of the feature

    fn main(){
        pthread.pthread mythread = 0
        pthread.create($mythread, 0, fn(uptr)uptr{return 1}, 0)
        /. alternatively, using UCS which has automatic module namespace merging ./
        $mythread.create(0, fn(uptr)uptr{return 1}, 0)
    }