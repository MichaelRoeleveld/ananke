# related keywords

* `fun(in)ret`: a function taking parameters `in` and returning something of type `ret`
* `opr`: operator type. See section operators.
* `ret`: return to caller context

# function

Myfunc is a function that takes a pointer to an array of i32, a pointer to an i8, and returns a pointer to an array of i32.

	fun myfunc(i32[]* x, i8* y) i32[]* {
		; code	using x and y
	}

# operators

Similar to functions, except you have operator precedence (priority), no parentheses, and prefix, infix or postfix notation.

NOTE:
* an identifier may only be used for an operator OR a function (or something else)
* illegal to declare different operator kinds with the same identifier
* legal to overload the same operator kind with the same identifier for different types
* it is not possible to declare ternary or even higher arity operators, only unary and binary

* prio: priority, aka binding power, aka precedence
* * even: left-associative
* * odd: right-associative
* left: left operand
* right: right operand
* res: type of result

```ank
	; declaration
	opr prio infix (left) (right) res {/.code./}
	opr prio prefix () (right) res {/.code./}
	opr prio postfix (left) () res {/.code./}

	; usage
	x infix y
	prefix y
	x postfix

	; overloading
	opr + (left) (right) {/.code./}
	opr - () (right) {/.code./}
	opr ++ (left) () {/.code./}

	; operators to functions
	fun infix_fun = (infix)
	fun prefix_fun = (prefix)
	fun postfix_fun = (postfix)

	; usage
	infix_fun(x,y)
	prefix_fun(y)
	postfix_fun(x)

	; illegal overload
	opr 1 increment(let left) () {ret left + 1}
	opr 1 increment(let left) (let right) {/. this is illegal, increment can not both be a postfix and infix operator ./}
```
