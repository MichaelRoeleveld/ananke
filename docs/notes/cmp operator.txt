
; compare semantic versions

let maj, min, rel = ..."3.1.2".split(".")
check maj <=> 3 || min <=> 6 || rel <=> 8 == -1 else "Too old"

check [3,1,2] <=> "3.1.2".split(".") else "Too old"