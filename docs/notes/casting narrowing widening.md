# Warning: this article is not accurate and contains old syntax AND semantics

# Casting, Converting, Interpreting

	i32 src = -2
	u32 d1 = u32 src 
	u32 d1 = src as u32
	// interpret: copy the raw bits of "src" into the bits of "d1" 
	// or: change the type, but not the data

	u32 d2 = u32:src
	u32 d2 = src to u32
	// convert: properly turn src into an unsigned 32 bit integer.
	// or: change the type, and maybe change the data with it

	u8 a
	u32 b

	a = u8 b  // copy the first 8 bits of b into a
	b = u32 a // copy a into b, fill the remaining 24 bits with 0

# Narrowing, widening

* An expression has a type, either implicit or explicit.

* Everything in an expression is either compatible with or widened to that type.

* **Widening**: for sized types, such as the i* and u* and f* family, the whole expression type becomes the largest of the present types.

* In the case that a type can not be inferred, it must be supplied.

```
	var a = 3+5       // illegal
	var b = i32 (3+5) // explicit expression type
	i32 c = 3+5       // inferred from left hand side as i32
	var d = c+7       // inferred from c as i32
	var e = i8 5 + i32 7 // type is i32
```

Narrowing does not happen unless an explicit conversion or interpretation takes place.

	i8 x = c+5      // illegal because of incompatible types i8,i32
	i8 y = i8 (x+5) // ok
	i8 y = i8:(x+5) // even better

Detailed example:

We will add two large 32 bit numbers, then add that to an 8 bit number.

	u8  k = 5
	u32 l = 900
	u32 m = 1200
	var n = k + (l+m)

`(l+m)` is in braces, so evaluated first. Result: `u32 2100`

	var n = k + 2100

`k` is `u8`, while `2100` is `u32`. `k` is widened to `u32`. `n` is `u32`.

	var n = u32 5 + u32 2100 = u32 2105