equirecursive types

Gen[T] = {T val, fn()Gen[T] next} | void none

this is a Gen[int] (using continuations)
fn geomseq(int start, int factor, int n){
	int x = start
	for i in 0..n {
		ret x, fun(){continue}
		x *= factor
	}
	ret nil	
}

This is also a Gen[int] (using recursion)
fn geomsq(int start, int factor, int n){
	ret n==0 then nil else start, \\geomsq(start*factor, factor, n-1)
}

This is also a Gen[int] (using closures)
fn foo(){
	ret 1, fun(){ret 2, fun(){ret 3, fun(){ret 4, fun(){ret nil}}}}
}
