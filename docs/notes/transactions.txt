atom x = 4

; acquire mutex on x
transact x {
	x++
	x--
	print(x)
}
; release