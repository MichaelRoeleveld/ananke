x: an expression
(x): a parenthesized expression
{c}: a block containing the code c
[e]: an array containing the element e

break: go just past the end of the current block (or parent block if the current block is a branch)
redo: go to the top of the current block (or parent block if the current block is a branch)

	{
		break
	} ; goes here

	{
		if 1 {
			break
		}
	} ; still goes here (branch)

	{
		if 1 {
			{
				break
			} ; goes here 
		}
	}

	{ ; goes here
		redo
	}

skip: immediately go to the next loop iteration

	for x in 0..5 {
		x == 4 then skip else print(x)
		print(" mississippi ")
	}
	// 1 mississippi 2 mississippi 3 mississippi 5 mississippi

fall: go to the next case/condition handler

	if x==0 {
		printl("only prints when x==0")
		fall
	} elif x==1 {
		printl("only prints when x==1 or x==0")
		fall
	} else {
		printl("always prints")
	}

ret
	go back to caller with value

label x
	declare x as a label
goto x
	go to label x. Must have been declared in current scope or a parent scope.

x and y
	if x==false, y is not checked (short circuit)

x or y
	if x==true, y is not checked (short circuit)

x imp y
	short circuiting logical implication
	(!x or y)
	y is only checked if x is true

x also y
	evaluate x and y in order and return x
	chains: `x also y also z` returns x as well

{x y}
	evaluate x and y in order and return y
	chains: `{x y z}` returns z

x?:a::b
	x: x=a then a else b

	name?:nil::"defaultName"

x??y
	if x==nil, y is used instead of x

x?
	if x may be nil, `x?` means the certainly not-nil case.

while x {c}
	check if x==true, then run c, then repeat
while x {c} else {c2}
	once x==false, run c2

{c} until x
	run c, then check if x==false, then repeat
{c} until x else {c2}
	once x==true, run c2

for x in xs {c}
	run c for every x in xs, ordered
for x in xs {c} else {c2}
	once the final iteration is over, run c2

note: else clauses are useful in loops, since they can be skipped with the break keyword.

x then c else c2
	run c if x==true
	run c2 if x==false

if x {c} elif x2 {c2} else {c3}
	run c if x==true
	run c2 if x==false and x2==true
	run c3 if x==false and x2==false

case x {m: c, m2: c2, _: c3}
x of {m: c, m2: c2, _: c3}
	same as `if x==m {c} elif x==m2 {c2} else {c3}`

check c
	if c==false, abort program

x where {c}
	check c whenever x is modified or instantiated

defer c
	run c at the end of the block (or when you leave the block by break, return, goto, skip)
	note: c is also run when the block is left for a continuation that doesn't eventually return.

eval c
	run c at compile time instead

let f = async c
	run c in parallel and give back a future
let v = await f
	wait for the future to arrive
	analogous to `let v=c`

async/await
	; fork/join multithreading model
	let futures = 0..8.map(\x async x also print("hello from thread" .. x))
	; an array of future[int]
	for f in futures print("thread " .. await f .. " awaited")

atom
	atom {
		; one thread per time will execute this code block
	}
	atom x; x is atomic, all loads of x and (compound) assignments to x are atomic.

hold
	atom x

	hold x {
		; the lock on x is acquired
	} ; released

let: declare a new constant
var: declare a new variable
type: declare a new type
where: refinement type
isa: interface

	iface IYellable {
		fun yell(it)
	}
	fun yell(str s){
		s.toUpper().."!!!".print()
	}
	fun foo(x isa IYellable){
		yell(x)
	}
	foo("Hello"); HELLO!!!
	foo(4); error

now
	current system time in nanoseconds

cc, cr
	current continuation/resumption

	cc == \\{continue}
	cr == \\{resume}

me
	current thread

self
	for recursing in an anonymous function

	let fib = \n n<2 then 1 else self(n-1)+self(n-2)

typeself
	for recursing in an anonymous type

	type list[T] = T, (typeself | nil)

it
	for refering to the instance of a type inside an interface or `where` clause

	type even = int where it %% 2; divides 2

this
	implicit first argument, pointer to the instance of the type, implicitly dereferenced on usage

	type MyClass {
		int x
		fun getX(){
			ret this.x
		}
		fun setX(int newX){
			this.x = newX
		}
	}

	MyClass obj: {4}
	obj.getX().print()
	obj.setX(42)
	obj.getX().print()
	