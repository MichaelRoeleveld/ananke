Consider the following code. Imagine that the code of `foo` and of `type T` is compiled separately and then linked later (maybe even dynamically).
```ml
/. foo_lib.ank ./
fn foo(let p){
    p.x = 3
    p.y = 1
    p.swap()
}

/. foo_use.ank ./
type T {
    int x,
    int y,
    int z,
    fn swap(){
        z = x
        x = y
        y = z
    },
    fn extraneous(){
        x = -1
        y = 1
    }
}

T t = (0,0,0)
foo(t)
```

Foo gets type-inferred with the following protocol:

```ml
prot P {
    int x,
    int y,
    fn swap()
}

fn foo(P p){
    p.x = 3
    p.y = 1
    p.swap()
}
```

NB: in reality, instead of `int`, `x` and `y` would be `Num` (another protocol that describes the implementation of `Num + Num`, etc). But for simplicity sake I'm demonstrating only a single translation "layer".

And `foo(t)` is compiled as (C syntax)

```c
/* foo_lib.c */
// offset table
// protocol member to actual member mapping
struct P { 
    unsigned char *impl;
    int x;
    int y;
    int swap;
};

typedef void (*fnptr)(void *this);
void foo(struct P p){
    *(int*)(p.impl+p.x) = 3;
    *(int*)(p.impl+p.y) = 1;
    ((fnptr)(p.impl+p.swap)) (p.impl);
}

/* foo_use.c */
struct T { };

struct T t = { };
// assuming base addr of t is higher than its fields bcuz stack
foo( (struct P){.impl=&t, .x = &t-&t.x, .y = &t-&t.y, .swap=&t-&t.swap} );
```